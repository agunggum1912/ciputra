<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="../gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="../plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include '../koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai user terlebih dahulu"); location.href="../logout.php"</script>';
  }

  $sql = "SELECT id, nama, email, foto FROM tb_user WHERE email='$_SESSION[userlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);
  $iduser = $row['id'];

  $nik = $_GET['nik'];

  $sqlkon1 = mysqli_query($koneksi,"SELECT a.id, a.mulai_penilaian, a.selesai_penilaian, a.hasil, a.id_kontrak, a.status_penilaian, b.nama_karyawan, b.nik, b.tgl_masuk, c.status_kontrak, c.banyak_penilaian FROM tb_penilaian AS a INNER JOIN tb_karyawan AS b ON a.id_karyawan=b.id INNER JOIN tb_kontrak AS c ON a.id_kontrak=c.id WHERE a.hasil!='' && b.nik='$nik' && c.status_kontrak=1")or die("Query kon1 Salah");
  $no1 = 1;
  $no_1 = 1;
  $hasil1 = mysqli_num_rows($sqlkon1);

  $sqlkon2 = mysqli_query($koneksi,"SELECT a.id, a.mulai_penilaian, a.selesai_penilaian, a.hasil, a.id_kontrak, a.status_penilaian, b.nama_karyawan, b.nik, b.tgl_masuk, c.status_kontrak, c.banyak_penilaian FROM tb_penilaian AS a INNER JOIN tb_karyawan AS b ON a.id_karyawan=b.id INNER JOIN tb_kontrak AS c ON a.id_kontrak=c.id WHERE a.hasil!='' && b.nik='$nik' && c.status_kontrak=2")or die("Query kon2 Salah");
  $no2 = 1;
  $no_2 = 1;
  $hasil2 = mysqli_num_rows($sqlkon2);

  $sqlkon3 = mysqli_query($koneksi,"SELECT a.id, a.mulai_penilaian, a.selesai_penilaian, a.hasil, a.id_kontrak, a.status_penilaian, b.nama_karyawan, b.nik, b.tgl_masuk, c.status_kontrak, c.banyak_penilaian FROM tb_penilaian AS a INNER JOIN tb_karyawan AS b ON a.id_karyawan=b.id INNER JOIN tb_kontrak AS c ON a.id_kontrak=c.id WHERE a.hasil!='' && b.nik='$nik' && c.status_kontrak=3")or die("Query kon3 Salah");
  $no3 = 1;
  $no_3 = 1;
  $hasil3 = mysqli_num_rows($sqlkon3);

  // $sqlkaryawan = "SELECT id, nama_karyawan, nik, tgl_masuk, status_karyawan, departemen_karyawan, posisi_karyawan, golongan, lokasi, id_user1, id_user2 FROM tb_karyawan WHERE nik='$nik'";
  // $qrykaryawan = mysqli_query($koneksi, $sqlkaryawan) or die ("Query karyawan salah!");
  // $rowkaryawan = mysqli_fetch_array($qrykaryawan);
  // $id = $rowkaryawan['id'];
  // $nama_karyawan = $rowkaryawan['nama_karyawan'];
  // $iduser1 = $rowkaryawan['id_user1'];
  // $iduser2 = $rowkaryawan['id_user2'];
  // $tglmasuk = date("d-m-Y", strtotime($rowkaryawan['tgl_masuk']));

  // $sqlkontrak1 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk1p1, b.selesai_penilaian AS spk1p1, b.hasil AS hk1p1, c.id_karyawan, c.mulai_penilaian AS mpk1p2, c.selesai_penilaian AS spk1p2, c.hasil AS hk1p2, d.id_karyawan, d.mulai_penilaian AS mpk1p3, d.selesai_penilaian AS spk1p3, d.hasil AS hk1p3, e.id_karyawan, e.mulai_penilaian AS mpk1p4, e.selesai_penilaian AS spk1p4, e.hasil AS hk1p4, f.id_karyawan, f.status_penilaian AS sp1, f.banyak_penilaian AS bp1 FROM tb_karyawan AS a LEFT JOIN tb_kon1_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon1_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon1_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon1_pen4 AS e ON a.id=e.id_karyawan LEFT JOIN tb_kontrak1 AS f ON a.id=f.id_karyawan WHERE a.nik='$nik'";
  // $qrykontrak1 = mysqli_query($koneksi, $sqlkontrak1);
  // $rowkontrak1 = mysqli_fetch_array($qrykontrak1);

  // $sqlkontrak2 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk2p1, b.selesai_penilaian AS spk2p1, b.hasil AS hk2p1, c.id_karyawan, c.mulai_penilaian AS mpk2p2, c.selesai_penilaian AS spk2p2, c.hasil AS hk2p2, d.id_karyawan, d.mulai_penilaian AS mpk2p3, d.selesai_penilaian AS spk2p3, d.hasil AS hk2p3, e.id_karyawan, e.mulai_penilaian AS mpk2p4, e.selesai_penilaian AS spk2p4, e.hasil AS hk2p4, f.id_karyawan, f.status_penilaian AS sp2, f.banyak_penilaian AS bp2 FROM tb_karyawan AS a LEFT JOIN tb_kon2_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon2_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon2_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon2_pen4 AS e ON a.id=e.id_karyawan LEFT JOIN tb_kontrak2 AS f ON a.id=f.id_karyawan WHERE a.nik='$nik'";
  // $qrykontrak2 = mysqli_query($koneksi, $sqlkontrak2);
  // $rowkontrak2 = mysqli_fetch_array($qrykontrak2);

  // $sqlkontrak3 = "SELECT a.id, a.nama_karyawan, a.nik, b.id_karyawan, b.mulai_penilaian AS mpk3p1, b.selesai_penilaian AS spk3p1, b.hasil AS hk3p1, c.id_karyawan, c.mulai_penilaian AS mpk3p2, c.selesai_penilaian AS spk3p2, c.hasil AS hk3p2, d.id_karyawan, d.mulai_penilaian AS mpk3p3, d.selesai_penilaian AS spk3p3, d.hasil AS hk3p3, e.id_karyawan, e.mulai_penilaian AS mpk3p4, e.selesai_penilaian AS spk3p4, e.hasil AS hk3p4, f.id_karyawan, f.status_penilaian AS sp3, f.banyak_penilaian AS bp3 FROM tb_karyawan AS a LEFT JOIN tb_kon3_pen1 AS b ON a.id=b.id_karyawan LEFT JOIN tb_kon3_pen2 AS c ON a.id=c.id_karyawan LEFT JOIN tb_kon3_pen3 AS d ON a.id=d.id_karyawan LEFT JOIN tb_kon3_pen4 AS e ON a.id=e.id_karyawan LEFT JOIN tb_kontrak3 AS f ON a.id=f.id_karyawan WHERE a.nik='$nik'";
  // $qrykontrak3 = mysqli_query($koneksi, $sqlkontrak3);
  // $rowkontrak3 = mysqli_fetch_array($qrykontrak3);


  // $sqluser = "SELECT id, nama, departemen FROM tb_user ORDER BY nama ASC";
  // $qryuser = mysqli_query($koneksi, $sqluser);

  // $sql_user1 = "SELECT id, nama, departemen FROM tb_user WHERE id='$iduser1'";
  // $qry_user1 = mysqli_query($koneksi, $sql_user1);
  // $row_user1 = mysqli_fetch_array($qry_user1);

  // $sqluser2 = "SELECT id, nama, departemen FROM tb_user ORDER BY nama ASC";
  // $qryuser2 = mysqli_query($koneksi, $sqluser2);

  // $sql_user2 = "SELECT id, nama, departemen FROM tb_user WHERE id='$iduser2'";
  // $qry_user2 = mysqli_query($koneksi, $sql_user2);
  // $row_user2 = mysqli_fetch_array($qry_user2);

  ?>

  
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <span><?php echo $_SESSION['userlogin']; ?></span>
            <i class="fas fa-user-alt"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="dropdown-divider"></div>
            <a href="settinguser.php" class="dropdown-item">
              <i class="fas fa-cog mr-2"></i>
              <span class="float-right text-muted text-sm">Setting</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="../logout.php" class="dropdown-item">
              <i class="fas fa-sign-out-alt mr-2"></i>
              <span class="float-right text-muted text-sm">Logout</span>
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-olive elevation-4">
      <!-- Brand Logo -->
      <a href="homeuser.php" class="brand-link navbar-light">
        <img src="../gambar/logociputra.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <?php
            $cek_foto = $row['foto'];
            $tempat_foto = '../foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='../foto/blank.png'></a>";
            }
            ?>
          </div>
          <div class="info">
            <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="assuser.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewassuser.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="settinguser.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Assessment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homeuser.php">Home</a></li>
              <li class="breadcrumb-item"><a href="viewassuser.php">View Assessment</a></li>
              <li class="breadcrumb-item active">Edit Assessment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 1</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <?php if($hasil1 > 0){?>
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                  <?php };?>
                  <?php while ($rowkon1=mysqli_fetch_array($sqlkon1)) {
                    if ($rowkon1['status_kontrak'] == 1) {?>
                      <tbody>
                        <tr>
                          <td><?php echo $no1++ ;?></td>
                          <td><?php echo $rowkon1['nama_karyawan'];?></td>
                          <td><?php echo $rowkon1['nik'];?></td>
                          <td>Assessment <?php echo $rowkon1['status_penilaian'];?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon1['mulai_penilaian']));?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon1['selesai_penilaian']));?></td>
                          <td><center><?php echo $rowkon1['hasil'];?></center></td>
                          <td>
                            <div style="padding-left: 12px;" class="btn-group">
                              <a href="<?php if($rowkon1['banyak_penilaian'] == $rowkon1['status_penilaian'] && $rowkon1['hasil']){echo "editproses2";}else{echo "editproses";};?>.php?id=<?php echo $rowkon1['id'];?>" class="btn btn-info"><i class='fas fa-edit'></i></a>
                              <a href="<?php if($rowkon1['banyak_penilaian'] == $rowkon1['status_penilaian'] && $rowkon1['hasil']){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $rowkon1['id'];?>" target='blank' class="btn btn-primary"><i class='fas fa-print'></i></a>
                            </div>
                          </td>
                        </tr>
                      <?php };?>
                    <?php };?>
                  </tbody>
                </table>
                <b>*Lihat paling kanan untuk proses Edit/Print</b>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>


        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 2</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <?php if($hasil2 > 0){?>
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                  <?php };?>
                  <?php while ($rowkon2=mysqli_fetch_array($sqlkon2)) {
                    if ($rowkon2['status_kontrak'] == 2) {?>
                      <tbody>
                        <tr>
                          <td><?php echo $no2++ ;?></td>
                          <td><?php echo $rowkon2['nama_karyawan'];?></td>
                          <td><?php echo $rowkon2['nik'];?></td>
                          <td>Assessment <?php echo $rowkon2['status_penilaian'];?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon2['mulai_penilaian']));?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon2['selesai_penilaian']));?></td>
                          <td><center><?php echo $rowkon2['hasil'];?></center></td>
                          <td>
                            <div style="padding-left: 12px;" class="btn-group">
                              <a href="<?php if($rowkon2['banyak_penilaian'] == $rowkon2['status_penilaian'] && $rowkon2['hasil']){echo "editproses2";}else{echo "editproses";};?>.php?id=<?php echo $rowkon2['id'];?>" class="btn btn-info"><i class='fas fa-edit'></i></a>
                              <a href="<?php if($rowkon2['banyak_penilaian'] == $rowkon2['status_penilaian'] && $rowkon2['hasil']){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $rowkon2['id'];?>" target='blank' class="btn btn-primary"><i class='fas fa-print'></i></a>
                            </div>
                          </td>
                        </tr>
                      <?php };?>
                    <?php };?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 3</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <?php if($hasil3 > 0){?>
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                  <?php };?>
                  <?php while ($rowkon3=mysqli_fetch_array($sqlkon3)) {
                    if ($rowkon3['status_kontrak'] == 3) {?>
                      <tbody>
                        <tr>
                          <td><?php echo $no3++ ;?></td>
                          <td><?php echo $rowkon3['nama_karyawan'];?></td>
                          <td><?php echo $rowkon3['nik'];?></td>
                          <td>Assessment <?php echo $rowkon3['status_penilaian'];?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon3['mulai_penilaian']));?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon3['selesai_penilaian']));?></td>
                          <td><center><?php echo $rowkon3['hasil'];?></center></td>
                          <td>
                            <div style="padding-left: 12px;" class="btn-group">
                              <a href="<?php if($rowkon3['banyak_penilaian'] == $rowkon3['status_penilaian'] && $rowkon3['hasil']){echo "editproses2";}else{echo "editproses";};?>.php?id=<?php echo $rowkon3['id'];?>" class="btn btn-info"><i class='fas fa-edit'></i></a>
                              <a href="<?php if($rowkon3['banyak_penilaian'] == $rowkon3['status_penilaian'] && $rowkon3['hasil']){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $rowkon3['id'];?>" target='blank' class="btn btn-primary"><i class='fas fa-print'></i></a>
                            </div>
                          </td>
                        </tr>
                      <?php };?>
                    <?php };?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>




      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="../plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
      format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
</body>
</html>
