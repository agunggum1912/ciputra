<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="../gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="../assets/css/style.css">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include '../koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['userlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai user terlebih dahulu"); location.href="../logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[userlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);

  if (isset($_POST['upload'])) {
    $ekstensi_diperbolehkan = array('png','jpg','jpeg');
    $nama = $_FILES['foto']['name'];
    $x = explode('.', $nama);
    $ekstensi = strtolower(end($x));
    $ukuran = $_FILES['foto']['size'];
    $file_tmp = $_FILES['foto']['tmp_name'];  
 
      
    if (empty($_FILES['foto']['name'])) {
      echo "<script>alert('Anda belum pilih foto, silahkan pilih file foto yang ingin anda Upload!');history.go(-1)</script>";
    }elseif(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
      if($ukuran < 1044070){
        $nama = date("YmdHis").$nama;
        move_uploaded_file($file_tmp, '../foto/'.$nama);
        $sql2 = "UPDATE tb_user SET foto='$nama' WHERE email='$_SESSION[userlogin]'";
        $qry2 = mysqli_query($koneksi, $sql2);
        if($qry2){
          echo "<script>alert('File Berhasil di Upload');window.location='settinguser.php'; </script>";
        }else{
          echo "<script>alert('GAGAL UPLOAD GAMBAR!'); </script>";
        }
      }else{
        echo "<script>alert('Ukuran File Terlalu Besar!');</script>";
      }
    }else{
      echo "<script>alert('Ekstensi File yang di Upload tidak di perbolehkan!');</script>";
    }
  }
 

  if (isset($_POST['update'])) {
    $passlama = $_POST['passlama'];
    $passbaru = trim($_POST['passbaru']);
    $passbaru2 = $_POST['passbaru2'];
  
    $passlama2  = (md5($passlama));
    $passbaru3  = (md5($passbaru));
    $cek    = mysqli_query($koneksi, "SELECT email, password FROM tb_user WHERE password='$passlama2' AND email='$_SESSION[userlogin]'");
    $cek2     = mysqli_num_rows($cek);

  
    // $uppercase = preg_match("/[A-Z]/", $passbaru);
    $lowercase = preg_match("/[a-zA-Z]/", $passbaru);
    $number = preg_match("/[0-9]/", $passbaru);

    if (empty($passlama) && empty($passbaru) && empty($passbaru2)) {
      echo "<script>alert('Anda tidak mengisi data apapun di Change Password!');history.go(-1)</script>";
    }elseif (empty($passlama)) {
      echo "<script>alert('Silahkan masukkan Password anda!');history.go(-1)</script>";
    }elseif (empty($passbaru)) {
      echo "<script>alert('Silahkan masukkan New Password anda!');history.go(-1)</script>";
    }elseif (empty($passbaru2)) {
      echo "<script>alert('Silahkan masukkan Confirmation Password anda!');history.go(-1)</script>";
    }elseif($passlama2 == $passbaru3){
      echo "<script>alert('Password baru tidak boleh sama dengan password yang lama!');history.go(-1)</script>";
    }elseif($cek2 >= 1){
      if($lowercase && $number && strlen($passbaru) >= 6){
        if ($passbaru == $passbaru2) {
        
          $update   = mysqli_query($koneksi, "UPDATE tb_user SET password='$passbaru3' WHERE email='$_SESSION[userlogin]'");
          if ($update) {
            echo "<script>alert('Password telah berhasil berubah.');window.location='settinguser.php'; </script>";
          }else{
            echo "<script>alert('Gagal merubah password!');</script>";
          }
        }else{
          echo "<script>alert('Confirm Password tidak sama!');</script>";
        }
      }else{
        echo "<script>alert('Password  harus mengandung angka dan huruf, tidak boleh special character & lebih 6 panjang character!');</script>";
      }
    }else{
      echo "<script>alert('Password lama tidak cocok!');</script>";
    }
  }


  ?>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['userlogin']; ?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="settinguser.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="../logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="homeuser.php" class="brand-link navbar-light">
      <img src="../gambar/logociputra.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row['foto'];
            $tempat_foto = '../foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='../foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="assuser.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewassuser.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="settinguser.php" class="nav-link active">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Setting</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="homeuser.php">Home</a></li>
              <li class="breadcrumb-item active">Setting</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-5">

            <!-- TABLE: LATEST ORDERS -->
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Change Picture</h3>

              </div>
              <!-- /.card-header -->
              <form action="" method="post" enctype="multipart/form-data">
                <div style="border-top: solid #00690a; border-bottom: solid #dee2e6;" class="card-body p-0">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <!-- <div style="padding-left: 50px;" class="col-md-12"> -->
                      <!-- Uploaded image area-->
                      <br>
                      <div class="image-area"><img id="imageResult" src="<?php echo $tempat_foto; ?>" alt="" class="img-fluid rounded shadow-sm mx-auto d-block"></div>
                      <div class="input-group border1 mt-4 mb-3 bg-white">
                      <!-- <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm"> -->
                        <input required id="upload" type="file" name="foto" onchange="readURL(this);" class="form-control border-0" >
                        <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file</label>
                        <div class="input-group-append">
                          <label for="upload" class="btn btn-abu m-0"> <i class="fas fa-cloud-upload-alt text-muted"></i><small class="text-uppercase font-weight-bold text-muted"> Choose file</small></label>
                        </div>
                      </div>
                      <!-- </div> -->
                    </div>
                    <span>*Hanya File "jpg & pbg" < 1mb</span>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                  <input onclick="return confirm('Foto sebelum akan terhapus apakah anda yakin ingin merubah foto?')" class="btn btn-primary float-right" type="submit" name="upload" value="Upload">
                </div>
                <input required type="hidden" name="id" value="">
              </form>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

          <div class="col-md-7">

            
            <!-- TABLE: LATEST ORDERS -->
            <form method="post" action="" name="gantipass">
              <div class="card card-olive">
                <div class="card-header">
                  <h3 class="card-title">Change Password</h3>

                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <div class="table-responsive">
                    <table border="0" class="table-setting m-0">
                      <tbody>
                      <tr style="border-top: solid #00690a;">
                        <td>Username</td>
                        <td>:</td>
                        <td>
                          <?php echo $_SESSION['userlogin']; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Password</td>
                        <td>:</td>
                        <td>
                          <input type="Password" name="passlama" class="form-control input_user border-list">
                        </td>
                      </tr>
                      <tr>
                        <td>New Password</td>
                        <td>:</td>
                        <td>
                          <input type="Password" name="passbaru" class="form-control input_user border-list">
                        </td>
                      </tr>
                      <tr style="border-bottom: solid #dee2e6;">
                        <td>Confirmation Password</td>
                        <td>:</td>
                        <td>
                          <input type="Password" name="passbaru2" class="form-control input_user border-list">
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                  <input onclick="return confirm('Anda yakin ingin merubah password?')" class="btn btn-primary float-right" type="submit" name="update" value="Update">
                </div>
                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </form>

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>
</div>


<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
});

/*  ==========================================
    SHOW UPLOADED IMAGE NAME
* ========================================== */
var input = document.getElementById( 'upload' );
var infoArea = document.getElementById( 'upload-label' );

input.addEventListener( 'change', showFileName );
function showFileName( event ) {
  var input = event.srcElement;
  var fileName = input.files[0].name;
  infoArea.textContent = 'File name: ' + fileName;
}
</script>

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="../plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="../plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
</body>
</html>
