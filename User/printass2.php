<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
	<title>Ciputra</title>



	<style type="text/css">
		body{
			font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
			-webkit-print-color-adjust: exact;
			padding: 80px 40px;
		}

		.logo{
			height: 50px;
			width: 75px;
		}

		.namalogo {
			position: absolute;
			margin-top: 10px;
			padding-left: 20px;
		}

		.namalogo2 {
			padding-left: 20px;
			text-align: center;
			font-size: 18pt;
			font-weight: bold;
		}

		h3 {
			margin: 0;
		}

		.garis {
			border-top: 2px solid #000;
			padding-top: 10px;
			padding-bottom: 20px; 
		}

		.X {
			border:1px solid #000;
			padding: 0px 8px;
			margin-left: 20px;
			margin-right: 20px;
		}

		.X2 {
			border:1px solid #000;
			padding: 0px 8px;
			margin-right: 40px;
		}

	</style>

	<?php
	include 'koneksi.php';

	// mengaktifkan session
	session_start();
	if (!isset($_SESSION['userlogin'])) {
	// if($_SESSION['status'] != "login") {
		echo '<script language="javascript">alert("Dilarang akses, login sebagai user terlebih dahulu"); location.href="logout.php"</script>';
	}

	$sql = "SELECT id, nama, email, foto FROM tb_user WHERE email='$_SESSION[userlogin]'";
	$qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
	$row = mysqli_fetch_array($qry);
	$iduser = $row['id'];

	if (isset($_GET['id'])) {
		$id = $_GET['id'];

		$sql2 = mysqli_query($koneksi,"SELECT a.id AS id_penilaian, a.*, b.mulai_kontrak, b.selesai_kontrak, b.banyak_penilaian, b.status_kontrak, c.nik, c.nama_karyawan, c.id_user2, c.tgl_masuk, c.lokasi, c.departemen_karyawan, c.posisi_karyawan, c.golongan, c.tgl_buat, d.nama FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id INNER JOIN tb_karyawan AS c ON c.id=b.id_karyawan INNER JOIN tb_user AS d ON c.id_user1=d.id WHERE a.id='$id'")or die("Query 2 Salah!");
		$row2 = mysqli_fetch_array($sql2);

		$sql3 = mysqli_query($koneksi,"SELECT a.id AS id_penilaian, b.id_user2, c.nama FROM tb_penilaian AS a INNER JOIN tb_karyawan AS b ON b.id=a.id_karyawan INNER JOIN tb_user AS c ON b.id_user2=c.id WHERE a.id='$id'")or die("Query 3 Salah!");
		$row3 = mysqli_fetch_array($sql3);

		if ($row2['banyak_penilaian'] != $row2['status_penilaian']) {
      		echo "<script>alert('Upss! Saat ini halaman tidak tersedia!');history.go(-1)</script>";
    	}
	}
	?>



</head>
<body>
	<div style="page-break-after:always;">
		<img src="gambar/logociputra.svg" class="logo">
		<span class="namalogo">Ciputra Group</span>

		<h3><center>PENILAIAN PRESENTASI KARYAWAN DALAM MASA <br> PERJANJIAN KERJA</center></h3>
		<span><center>(Untuk dikirimkan kembali kepada departemen Sumber Daya Manusia)</center></span>
		<br>
		<table border="0">
			<tr>
				<td width="100px">Kepada Yth</td>
				<td>:</td>
				<td>Bpk/Ibu. <?php echo $row2['nama'];?></td>
			</tr>
			<tr>
				<td width="100px">Dari</td>
				<td>:</td>
				<td>Departemen Sumber Daya Manusia</td>
			</tr>
			<tr>
				<td>Tanggal</td>
				<td>:</td>
				<td><?php echo date("d-M-Y", strtotime($row2['tgl_buat']));?></td>
			</tr>
		</table>

		<br>
		<div class="garis">
			<span>Sehubungan dengan kegiatan evaluasi periodik 3 bulanan terhadap karyawan yang masih dalam status perjanjian kerja, maka dengan ini kami sampaikan lembar isian penilaian presentasi dari:</span>
		</div>

		<table border="0">
			<tr>
				<td width="150px">Nama Karyawan</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['nama_karyawan'];?></td>
				<td width="100px">NIK</td>
				<td>:</td>
				<td><?php echo $row2['nik'];?></td>
			</tr>
			<tr>
				<td width="150px">Departemen</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['departemen_karyawan'];?></td>
				<td width="100px">Golongan</td>
				<td>:</td>
				<td><?php echo $row2['golongan'];?></td>
			</tr>
			<tr>
				<td>Jabatan</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['posisi_karyawan'];?></td>
				<td width="100px">Tgl Masuk</td>
				<td>:</td>
				<td><?php echo date("d-M-Y", strtotime($row2['tgl_masuk']));?></td>
			</tr>
			<tr>
				<td width="150px">Lokasi</td>
				<td>:</td>
				<td width="300px"><?php echo $row2['lokasi'];?></td>
			</tr>
		</table>
		<table border="0">
			<tr>
				<td width="150px">Status Kepegawaian</td>
				<td>:</td>
				<td><span class="X"><?php if($row2['status_kontrak'] == 1){echo "X";}else{echo "_";}?></span></td>
				<td width="100px">Kontrak 1</td>
				<td>tanggal</td>
				<td width="120px"><center><u><?php if($row2['status_kontrak'] == 1)echo date("d-M-Y", strtotime($row2['mulai_kontrak']));?></u></center></td>
				<td>s/d</td>
				<td width="120px"><center><u><?php if($row2['status_kontrak'] == 1)echo date("d-M-Y", strtotime($row2['selesai_kontrak']));?></u></center></td>
			</tr>
			<tr>
				<td width="150px"></td>
				<td></td>
				<td><span class="X"><?php if($row2['status_kontrak'] == 2){echo "X";}else{echo "_";}?></span></td>
				<td width="100px">Kontrak 2</td>
				<td>tanggal</td>
				<td><center><u><?php if($row2['status_kontrak'] == 2)echo date("d-M-Y", strtotime($row2['mulai_kontrak']));?></u></center></td>
				<td>s/d</td>
				<td><center><u><?php if($row2['status_kontrak'] == 2)echo date("d-M-Y", strtotime($row2['selesai_kontrak']));?></u></center></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><span class="X"><?php if($row2['status_kontrak'] == 3){echo "X";}else{echo "_";}?></span></td>
				<td width="100px">Kontrak 3</td>
				<td>tanggal</td>
				<td><center><u><?php if($row2['status_kontrak'] == 3)echo date("d-M-Y", strtotime($row2['mulai_kontrak']));?></u></center></td>
				<td>s/d</td>
				<td><center><u><?php if($row2['status_kontrak'] == 3)echo date("d-M-Y", strtotime($row2['selesai_kontrak']));?></u></center></td>
			</tr>
		</table>
	</br>
	
	<span>Periode Penilaian:</span>
	<table border="0">
		<tr>
			<td><span class="X2"><?php if($row2['status_penilaian'] == 1){echo "X";}else{echo "_";}?></span></td>
			<td>3 bulan ke-1: tanggal</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 1)echo date("d-M-Y", strtotime($row2['mulai_penilaian']));?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 1)echo date("d-M-Y", strtotime($row2['selesai_penilaian']));?></u></center></td>
		</tr>
		<tr>
			<td><span class="X2"><?php if($row2['status_penilaian'] == 2){echo "X";}else{echo "_";}?></span></td>
			<td>3 bulan ke-2: tanggal</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 2)echo date("d-M-Y", strtotime($row2['mulai_penilaian']));?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 2)echo date("d-M-Y", strtotime($row2['selesai_penilaian']));?></u></center></td>
		</tr>
		<tr>
			<td><span class="X2"><?php if($row2['status_penilaian'] == 3){echo "X";}else{echo "_";}?></span></td>
			<td>3 bulan ke-3: tanggal</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 3)echo date("d-M-Y", strtotime($row2['mulai_penilaian']));?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 3)echo date("d-M-Y", strtotime($row2['selesai_penilaian']));?></u></center></td>
		</tr>
		<tr>
			<td><span class="X2"><?php if($row2['status_penilaian'] == 4){echo "X";}else{echo "_";}?></span></td>
			<td>3 bulan ke-4: tanggal</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 4)echo date("d-M-Y", strtotime($row2['mulai_penilaian']));?></u></center></td>
			<td>s/d</td>
			<td width="120px"><center><u><?php if($row2['status_penilaian'] == 4)echo date("d-M-Y", strtotime($row2['selesai_penilaian']));?></u></center></td>
		</tr>
	</table>
	<br>

	<span>Mohon Bapak/Ibu memberikan penilaian untuk periode penilaian yang sedang</span>
	<br>
	<br>
	<span>Terimakasih,</span>
	<br>
	<br>
	<span><b>&emsp;DAHLIANA</b></span>
	<br>
	<br>
	<span>Departemen SDM</span>
	<br>
	<p style="font-size: 12px; line-height: 14px;">
		Jl. Prof DR. Satrio kav.6<br>
		Jakarta 12940, INDONESIA<br>
		Tel: (62-21) 5225858,5226868,5207333<br>
		Fax: (62-21) 5274125, 5205262,5205886
	</p>
</div>
<div style="page-break-after:always;">
	<br>
	<br>
	<br>
	<br>

	<span style="font-size: 8pt;">LEMBAR PENILAIAN - akan terisi otomatis checkmark ( &radic; ) sesuai penilaian di sistem</span>
	<span style="font-size: 8pt; padding-left: 150px;">Golongan I-IV</span>

	<table border="1" style="border-collapse: collapse;">
		<tr>
			<td rowspan="3" width="230px"><center>ASPEK</center></td>
			<td colspan="5"><center>PENILAIAN</center></td>
		</tr>
		<tr>
			<td width="80px">RENDAH</td>
			<td width="80px">KURANG</td>
			<td width="80px">CUKUP</td>
			<td width="80px">BAIK</td>
			<td width="80px">TINGGI</td>
		</tr>
		<tr style="font-size: 8pt; line-height: 10px;">
			<td style="padding-top: 5px; padding-left: 5px;">Tidak Memenuhi Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Sebagian Memenuhi Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Memenuhi Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Melampaui Persyaratan</td>
			<td style="padding-top: 5px; padding-left: 5px;">Jauh Melampaui Persyaratan</td>
		</tr>
		<tr>
			<td colspan="6">A. KEHADIRAN</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Ketepatan <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Hadir pada waktu yang sudah ditetapkan
			</td>
			<td><center><?php if($row2['jawaban1'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban1'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban1'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban1'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban1'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Absensi <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Ketidakhadiran disebabkan oleh ijin atau alpha
			</td>
			<td><center><?php if($row2['jawaban2'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban2'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban2'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban2'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban2'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td colspan="6">B. PENGETAHUAN</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Pengetahuan Teoritis <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Pengetahuan teoritis tentang pekerjaan, yang <br> &nbsp;&nbsp;&nbsp;&nbsp;
				didapat dari pendidikan formal dan / informal
			</td>
			<td><center><?php if($row2['jawaban3'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban3'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban3'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban3'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban3'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Kemampuan menetapkan pengetahuan teoritis <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				dalam pelaksanaan pekerjaan, Kemampuan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				kerja praktis terapan
			</td>
			<td><center><?php if($row2['jawaban4'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban4'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban4'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban4'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban4'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				3. Kemampuan Mempelajari Hal Baru <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kemampuan untuk beradaptasi, mempelajari <br> &nbsp;&nbsp;&nbsp;&nbsp;
				dan melaksanakan hal-hal baru dalam pekerjaan, <br> &nbsp;&nbsp;&nbsp;&nbsp;
				motivasi untuk menemukan hal baru
			</td>
			<td><center><?php if($row2['jawaban5'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban5'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban5'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban5'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban5'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td colspan="6">C. HASIL KERJA</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Kualitas Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Keterampilan, ketelitian, kecermatan dan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				kerapihan hasil kerja, termasuk tingkat <br> &nbsp;&nbsp;&nbsp;&nbsp;
				pengawasan dan penelitian ulang yang <br> &nbsp;&nbsp;&nbsp;&nbsp;
				diperlukan
			</td>
			<td><center><?php if($row2['jawaban6'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban6'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban6'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban6'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban6'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Kuantitas Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Jumlah / Volume kerja (output) yang <br> &nbsp;&nbsp;&nbsp;&nbsp;
				diselesaikan dalam jangka waktu tertentu, <br> &nbsp;&nbsp;&nbsp;&nbsp;
				termasuk kecepatan kera dan tingkat <br> &nbsp;&nbsp;&nbsp;&nbsp;
				pengawasan yang diperlukan
			</td>
			<td><center><?php if($row2['jawaban7'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban7'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban7'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban7'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban7'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td colspan="6">C. SIKAP KERJA</td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				1. Cara Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kemauan untuk bekerja secara sistematis dan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				efisien
			</td>
			<td><center><?php if($row2['jawaban8'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban8'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban8'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban8'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban8'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				2. Komitmen <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Rasa tanggung jawab terhadap pekerjaan, <br> &nbsp;&nbsp;&nbsp;&nbsp;
				tingkat pengutamaan kepentingan pekerjaan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				diatas kepentingan lainnya
			</td>
			<td><center><?php if($row2['jawaban9'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban9'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban9'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban9'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban9'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				3. Kerja Sama <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kesediaan bekerja sama dan membantu <br> &nbsp;&nbsp;&nbsp;&nbsp;
				memberi dan menerima masukan ke/dari <br> &nbsp;&nbsp;&nbsp;&nbsp;
				atasan/rekan kerja/bawahan/orang lain
			</td>
			<td><center><?php if($row2['jawaban10'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban10'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban10'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban10'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban10'] == 5) echo "&radic;";?></center></td>
		</tr>
		<tr>
			<td style="font-size: 8pt; padding: 5px 0px;">
				4. Keselamatan Kerja <br> &nbsp;&nbsp;&nbsp;&nbsp; 
				Kesediaan untuk mementingkan keselamatan <br> &nbsp;&nbsp;&nbsp;&nbsp;
				kerja diri sendiri maupun orang lian, merawat <br> &nbsp;&nbsp;&nbsp;&nbsp;
				dan menggunakan peralatan kantor secara <br> &nbsp;&nbsp;&nbsp;&nbsp;
				efesien
			</td>
			<td><center><?php if($row2['jawaban11'] == 1) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban11'] == 2) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban11'] == 3) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban11'] == 4) echo "&radic;";?></center></td>
			<td><center><?php if($row2['jawaban11'] == 5) echo "&radic;";?></center></td>
		</tr>
	</table>
</div>
<div style="page-break-after:always;">
	<br>
	<br>
	<br>
	<br>

	<table border="1" style="display: block; font-size: 10pt; border-collapse: collapse;">
		<tr>
			<td align="left" valign="top" height="200" width="700"><b>Keterangan tentang prestasi atau unjuk kerja:</b><br>
			<?php echo $row2['ket1'] ?></td>
		</tr>
		<tr>
			<td align="left" valign="top" height="200"><b>Hal-hal penting yang terjadi:</b><br>
			<?php echo $row2['ket2'] ?></td>
		</tr>
	</table>
	<br>
	<br>
	<br>


	<span>Tanggal Penilaian:<u><?php if($row2['tgl_penilaian'] != "0000-00-00 00:00:00") echo date("d-M-Y", strtotime($row2['tgl_penilaian']));?></u></span>
	<br>
	<br>
	<br>

	<span><b>KESIMPULAN AKHIR: <?php echo $row2['hasil'];?></b></span><br>
	<span style="padding-left: 30px;">A = Memuaskan, jauh diatas rata-rata <br></span>
	<span style="padding-left: 30px;">B = Baik, diatas rata-rata <br></span>
	<span style="padding-left: 30px;">C = Cukup, tergolong rata-rata <br></span>
	<span style="padding-left: 30px;">D = Kurang memuaskan, dibawah rata-rata <br></span>
	<span style="padding-left: 30px;">E = Tidak memuaskan, jauh dibawah rata-rata</span>
	<br>
	<br>
	<br>
	<br>

	<table border="0">
		<tr>
			<td width="500" height="100" align="left" valign="top">Atasan,</td>
			<td align="left" valign="top">&nbsp;Mengetahui,</td>
		</tr>
		<tr>
			<td><?php echo $row2['nama'];?></td>
			<td>Manager SDM</td>
		</tr>
	</table>

</div>

<div style="page-break-after:always;">
	<br>
	<br>
	<br>
	<br>

	<img src="gambar/logociputra.svg" class="logo">
	<span class="namalogo2">PENILAIAN UNJUK KERJA KARYAWAN KONTRAK</span>
	<br>
	<br>

	<table border="0" style="font-size: 11pt;">
		<tr>
			<td width="300">Hasil penilaian untuk periode:</td>
			<td>- 3 bulan</td>
			<td>=</td>
			<td><b><?php if($row2['status_penilaian'] == 1) echo $row2['hasil'];?></b></td>
		</tr>
		<tr>
			<td rowspan="3" valign="top" style="font-size: 8pt;"><i>(Hasil akan terisi secara otomatis oleh sistem)</i></td>
			<td>- 6 bulan</td>
			<td>=</td>
			<td><b><?php if($row2['status_penilaian'] == 2) echo $row2['hasil'];?></b></td>
		</tr>
		<tr>
			<td>- 9 bulan</td>
			<td>=</td>
			<td><b><?php if($row2['status_penilaian'] == 3) echo $row2['hasil'];?></b></td>
		</tr>
		<tr>
			<td>- 12 bulan</td>
			<td>=</td>
			<td><b><?php if($row2['status_penilaian'] == 4) echo $row2['hasil'];?></b></td>
		</tr>
	</table>
	<br>
	<br>
	<br>

	<table border="0" style="display: block; font-size: 11pt; border-collapse: collapse;">
		<tr>
			<td align="left" valign="top" height="150"><b>Keterangan tentang prestasi atau unjuk kerja:</b><br>
			<?php echo $row2['ket3'];?>.</td>
		</tr>
	</table>

	<span>REKOMENDASI:</span>
	<table border="0" style="display: block; font-size: 11pt; border-collapse: collapse;">
		<tr>
			<td align="left" valign="top" width="50">A.</td>
			<td colspan="2"><?php if($row2['rekomendasi'] == "Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak."){echo "Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak.";}else{echo "<s>Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak.</s>";}?></td>
		</tr>
		<tr>
			<td></td>
			<td width="250">
				<?php
					if($row2['rekomendasi'] == "Diperpanjang kontrak selama 3 Bulan"){
						echo "Diperpanjang kontrak selama &emsp; <b><u>3</u></b>";
					}elseif ($row2['rekomendasi'] == "Diperpanjang kontrak selama 6 Bulan") {
						echo "Diperpanjang kontrak selama &emsp; <b><u>6</u></b>";
					}elseif ($row2['rekomendasi'] == "Diperpanjang kontrak selama 9 Bulan") {
						echo "Diperpanjang kontrak selama &emsp; <b><u>9</u></b>";
					}elseif ($row2['rekomendasi'] == "Diperpanjang kontrak selama 12 Bulan") {
						echo "Diperpanjang kontrak selama &emsp; <b><u>12</u></b>";
					}else{
						echo "<s>Diperpanjang kontrak selama</s>";
					}
				?>
			</td>
			<td><?php if(preg_match("/Diperpanjang kontrak selama/", $row2['rekomendasi'])){echo "Bulan";}else{echo "<s>Bulan</s>";}?></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2"><?php if($row2['rekomendasi'] == "Diberhentikan setelah habis masa kerja kontrak"){echo "Diberhentikan setelah habis masa kerja kontrak";}else{echo "<s>Diberhentikan setelah habis masa kerja kontrak</s>";};?></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="2">Lain-lain (sebutkan) ……………………………</td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<table border="0">
		<tr>
			<td width="200" height="100" align="left" valign="top">Jakarta, <?php if($row2['tgl_penilaian'] != "0000-00-00 00:00:00") echo date("d-M-Y", strtotime($row2['tgl_penilaian']));?></td>
			<td width="300"></td>
			<td width="200" height="100" align="left" valign="top"><?php if($row3['id_user2'] != 1) echo "Mengetahui / Menyetujui";?></td>
		</tr>
		<tr>
			<td style="border-bottom: 1px solid #000;"><?php echo $row2['nama'];?></td>
			<td></td>
			<?php if($row3['id_user2'] != 1) echo "<td style='border-bottom: 1px solid #000;'>".$row3['nama']."</td>";?>
		</tr>
		<tr>
			<td>Atasan Langsung</td>
			<td></td>
			<td><?php if($row3['id_user2'] != 1) echo "Atasan dari Atasan";?></td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<br>
	<br>

	<p style="font-size: 12px; line-height: 14px;">
		Jl. Prof DR. Satrio kav.6<br>
		Jakarta 12940, INDONESIA<br>
		Tel: (62-21) 5225858,5226868,5207333<br>
		Fax: (62-21) 5274125, 5205262,5205886
	</p>	

</div>




<script>
	window.print();
</script>


</body>
</html>