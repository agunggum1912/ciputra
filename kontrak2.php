<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);

  $id = $_GET['id_karyawan'];

  $sql2 = mysqli_query($koneksi,"SELECT nik FROM tb_karyawan WHERE id='$id'")or die("Query 2 Salah!");
  $row2 = mysqli_fetch_array($sql2);
  $nik = $row2['nik'];

  $sql3 = mysqli_query($koneksi,"SELECT a.id, a.banyak_penilaian, a.mulai_kontrak, a.selesai_kontrak, b.status_penilaian, MAX(b.status_penilaian) AS sp FROM tb_kontrak AS a INNER JOIN tb_penilaian AS b ON a.id=b.id_kontrak WHERE b.id_karyawan='$id' && a.status_kontrak=2")or die("Query 3 Salah!");
  $qry3 = mysqli_num_rows($sql3);
  $row3 = mysqli_fetch_array($sql3);
  $id_kontrak = $row3['id'];
  if (!empty($row3['status_penilaian'])) {
    $mk = $row3['mulai_kontrak'];
    $sk = $row3['selesai_kontrak'];

    function mkubah ($mk){
      $mkpisah = explode('-',$mk);
      $mkarray = array($mkpisah[2],$mkpisah[1],$mkpisah[0]);
      $mksatukan = implode('/',$mkarray);
      return $mksatukan;
    }
    $mk2 = mkubah($mk);

    function skubah ($sk){
      $skpisah = explode('-',$sk);
      $skarray = array($skpisah[2],$skpisah[1],$skpisah[0]);
      $sksatukan = implode('/',$skarray);
      return $sksatukan;
    }
    $sk2 = skubah($sk);
  }

  $sql4 = mysqli_query($koneksi,"SELECT id, mulai_penilaian, selesai_penilaian, status_penilaian FROM tb_penilaian WHERE id_kontrak='$id_kontrak'")or die("Query 4 salah!");
  $qry4 = mysqli_num_rows($sql4);
  if ($qry4 > 0) {
    while ($row4 = mysqli_fetch_array($sql4)) {
      $mulai_penilaian[] = $row4['mulai_penilaian'];
      $selesai_penilaian[] = $row4['selesai_penilaian'];
      $id_penilaian[] = $row4['id'];
    }
    $ubahmp = $mulai_penilaian[0];
    $ubahsp = $selesai_penilaian[0];

    function ubahmp ($ubahmp){
      $mppisah = explode('-',$ubahmp);
      $mparray = array($mppisah[2],$mppisah[1],$mppisah[0]);
      $mpsatukan = implode('/',$mparray);
      return $mpsatukan;
    }
    $ubahmp2 = ubahmp($ubahmp);

    function ubahsp ($ubahsp){
      $sppisah = explode('-',$ubahsp);
      $sparray = array($sppisah[2],$sppisah[1],$sppisah[0]);
      $spsatukan = implode('/',$sparray);
      return $spsatukan;
    }
    $ubahsp2 = ubahsp($ubahsp);
  }


  if (isset($_POST['submit'])) {
    $id2 = $_POST['id'];
    $mulai_kontrak = trim($_POST['mulai_kontrak']);
    $selesai_kontrak = trim($_POST['selesai_kontrak']);
    $status_karyawan = trim($_POST['status_karyawan']);
    $banyak_penilaian = trim($_POST['banyak_penilaian']);
    $mp1 = trim($_POST['mp1']);
    $sp1 = trim($_POST['sp1']);
    $mp2 = trim($_POST['mp2']);
    $sp2 = trim($_POST['sp2']);
    $mp3 = trim($_POST['mp3']);
    $sp3 = trim($_POST['sp3']);
    $mp4 = trim($_POST['mp4']);
    $sp4 = trim($_POST['sp4']);
    date_default_timezone_set('Asia/Jakarta');
    $time = date('Y-m-d H:i:s');

    // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    function ubahMK($mulai_kontrak){
      $pisahmk = explode('/',$mulai_kontrak);
      $arraymk = array($pisahmk[2],$pisahmk[1],$pisahmk[0]);
      $satukanmk = implode('-',$arraymk);
      return $satukanmk;
    }
    $mulai_kontrak2 = ubahMK($mulai_kontrak);

    function ubahSK($selesai_kontrak){
      $pisahsk = explode('/',$selesai_kontrak);
      $arraysk = array($pisahsk[2],$pisahsk[1],$pisahsk[0]);
      $satukansk = implode('-',$arraysk);
      return $satukansk;
    }
    $selesai_kontrak2 = ubahSK($selesai_kontrak);
    
    if (!empty($mp1)) {
      function ubahmp1($mp1){
        $pisahmp1 = explode('/',$mp1);
        $arraymp1 = array($pisahmp1[2],$pisahmp1[1],$pisahmp1[0]);
        $satukanmp1 = implode('-',$arraymp1);
        return $satukanmp1;
      } 
      $mp1_2 = ubahmp1($mp1);
    }

    if (!empty($sp1)) {
      function ubahsp1($sp1){
        $pisahsp1 = explode('/',$sp1);
        $arraysp1 = array($pisahsp1[2],$pisahsp1[1],$pisahsp1[0]);
        $satukansp1 = implode('-',$arraysp1);
        return $satukansp1;
      }
      $sp1_2 = ubahsp1($sp1);
    }
    
    if (!empty($mp2)) {
      function ubahmp2($mp2){
        $pisahmp2 = explode('/',$mp2);
        $arraymp2 = array($pisahmp2[2],$pisahmp2[1],$pisahmp2[0]);
        $satukanmp2 = implode('-',$arraymp2);
        return $satukanmp2;
      }
      $mp2_2 = ubahmp2($mp2);
    }
    
    if (!empty($sp2)) {
      function ubahsp2($sp2){
        $pisahsp2 = explode('/',$sp2);
        $arraysp2 = array($pisahsp2[2],$pisahsp2[1],$pisahsp2[0]);
        $satukansp2 = implode('-',$arraysp2);
        return $satukansp2;
      }
      $sp2_2 = ubahsp2($sp2);
    }
    
    if (!empty($mp3)) {
      function ubahmp3($mp3){
        $pisahmp3 = explode('/',$mp3);
        $arraymp3 = array($pisahmp3[2],$pisahmp3[1],$pisahmp3[0]);
        $satukanmp3 = implode('-',$arraymp3);
        return $satukanmp3;
      }
      $mp3_2 = ubahmp3($mp3);
    }
    
    if (!empty($sp3)) {
      function ubahsp3($sp3){
        $pisahsp3 = explode('/',$sp3);
        $arraysp3 = array($pisahsp3[2],$pisahsp3[1],$pisahsp3[0]);
        $satukansp3 = implode('-',$arraysp3);
        return $satukansp3;
      }
      $sp3_2 = ubahsp3($sp3);
    }
    
    if (!empty($mp4)) {
      function ubahmp4($mp4){
        $pisahmp4 = explode('/',$mp4);
        $arraymp4 = array($pisahmp4[2],$pisahmp4[1],$pisahmp4[0]);
        $satukanmp4 = implode('-',$arraymp4);
        return $satukanmp4;
      }
      $mp4_2 = ubahmp4($mp4);
    }
    
    if (!empty($sp4)) {
      function ubahsp4($sp4){
        $pisahsp4 = explode('/',$sp4);
        $arraysp4 = array($pisahsp4[2],$pisahsp4[1],$pisahsp4[0]);
        $satukansp4 = implode('-',$arraysp4);
        return $satukansp4;
      }
      $sp4_2 = ubahsp4($sp4);
    }

    
    if (empty($mulai_kontrak)) {
      echo "<script>alert('Start Contract harus di isi!');history.go(-1)</script>";
    }elseif (empty($selesai_kontrak)) {
      echo "<script>alert('Finish Contract Status harus di isi!');history.go(-1)</script>";
    }elseif (empty($status_karyawan)) {
      echo "<script>alert('Assessment Status harus di isi!');history.go(-1)</script>";
    }elseif (empty($banyak_penilaian)) {
      echo "<script>alert('Sum Assessment harus di isi!');history.go(-1)</script>";  
    }elseif($status_karyawan == "1"){
      if (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (!empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }elseif (!empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }elseif (!empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 telah terisi, silahkan hapus atau ganti Assessment Status jadi 2!');history.go(-1)</script>";
      }elseif (!empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 telah terisi, silahkan hapus atau ganti Assessment Status jadi 2!');history.go(-1)</script>";
      }else{
        if (!empty($row3['status_penilaian'])) {
          $sql5 = mysqli_query($koneksi,"UPDATE tb_kontrak SET id_proses=0, banyak_penilaian='$banyak_penilaian', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id='$id_kontrak'")or die("Query 5 salah!");
          $sql6 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id='$id_penilaian[0]'")or die("Query 6 Salah!");
          $sql7 = mysqli_query($koneksi,"DELETE FROM tb_penilaian WHERE id_kontrak='$id_kontrak' && status_penilaian='4'");
          $sql8 = mysqli_query($koneksi,"DELETE FROM tb_penilaian WHERE id_kontrak='$id_kontrak' && status_penilaian='3'");
          $sql9 = mysqli_query($koneksi,"DELETE FROM tb_penilaian WHERE id_kontrak='$id_kontrak' && status_penilaian='2'");
        }else{
          $sql10 = mysqli_query($koneksi,"INSERT INTO tb_kontrak (id_karyawan,status_kontrak,banyak_penilaian,id_proses,mulai_kontrak,selesai_kontrak)VALUES('$id',2,'$banyak_penilaian',0,'$mulai_kontrak2','$selesai_kontrak2')")or die("Query 10 Salah!");
          $sql11 = mysqli_query($koneksi,"SELECT a.id, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='1' && b.status_kontrak='2'")or die("Query 11 Salah!");
          $row11 = mysqli_fetch_array($sql11);
          $sql12 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2', tgl_buat='$time' WHERE id='$row11[id]'")or die("Query 12 Salah!");
        }
        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }elseif ($status_karyawan == "2") {
      if ($banyak_penilaian == "1" && $status_karyawan == "2") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (!empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }elseif (!empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 telah terisi, silahkan hapus atau ganti Assessment Status jadi 3!');history.go(-1)</script>";
      }else{
        if (!empty($row3['status_penilaian'])) {
          $sql13 = mysqli_query($koneksi,"UPDATE tb_kontrak SET id_proses=0, banyak_penilaian='$banyak_penilaian', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id='$id_kontrak'")or die("Query 13 salah!");
          $sql14 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id='$id_penilaian[0]'")or die("Query 14 salah!");
          $sql15 = mysqli_query($koneksi,"SELECT a.id, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='2' && b.status_kontrak='2'")or die("Query 15 Salah!");
          $qry15 = mysqli_num_rows($sql15);
          $row15 = mysqli_fetch_array($sql15);
          if($qry15 > 0){
            $sql16 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp2_2', selesai_penilaian='$sp2_2' WHERE id='$row15[id]'")or die("Query 16 salah!");
          }else{
            $sql17 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$id_kontrak',2,'$mp2_2','$sp2_2','$time')")or die("Query 17 salah!");
          }
          $sql18 = mysqli_query($koneksi,"DELETE FROM tb_penilaian WHERE id_kontrak='$id_kontrak' && status_penilaian='4'");
          $sql19 = mysqli_query($koneksi,"DELETE FROM tb_penilaian WHERE id_kontrak='$id_kontrak' && status_penilaian='3'");
        }else{
          $sql20 = mysqli_query($koneksi,"INSERT INTO tb_kontrak (id_karyawan,status_kontrak,banyak_penilaian,id_proses,mulai_kontrak,selesai_kontrak)VALUES('$id',2,'$banyak_penilaian',0,'$mulai_kontrak2','$selesai_kontrak2')");
          $sql21 = mysqli_query($koneksi,"SELECT a.id, a.id_kontrak, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='1' && b.status_kontrak='2'")or die("Query 20 Salah!");
          $row21 = mysqli_fetch_array($sql21);
          $sql22 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2', tgl_buat='$time' WHERE id='$row21[id]'")or die("Query 22 Salah!");
          $sql23 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$row21[id_kontrak]',2,'$mp2_2','$sp2_2','$time')")or die("Query 23 Salah!");
        }
        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }elseif ($status_karyawan == "3") {
      if ($banyak_penilaian == "1" && $status_karyawan == "3") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif ($banyak_penilaian == "2" && $status_karyawan == "3") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (!empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }elseif (!empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 telah terisi, silahkan hapus atau ganti Assessment Status jadi 4!');history.go(-1)</script>";
      }else{
        if (!empty($row3['status_penilaian'])) {
          $sql24 = mysqli_query($koneksi,"UPDATE tb_kontrak SET id_proses=0, banyak_penilaian='$banyak_penilaian', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id='$id_kontrak'")or die("Query 24 salah!");
          $sql25 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id='$id_penilaian[0]'")or die("Query 25 salah!");
          $sql26 = mysqli_query($koneksi,"SELECT a.id, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='2' && b.status_kontrak='2'")or die("Query 26 Salah!");
          $qry26 = mysqli_num_rows($sql26);
          $row26 = mysqli_fetch_array($sql26);
          if($qry26 > 0){
            $sql27 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp2_2', selesai_penilaian='$sp2_2' WHERE id='$row26[id]'")or die("Query 27 Salah!");
          }else{
            $sql28 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$id_kontrak',2,'$mp2_2','$sp2_2','$time')")or die("Query 28 salah!");
          }
          $sql29 = mysqli_query($koneksi,"SELECT a.id, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='3' && b.status_kontrak='2'")or die("Query 29 Salah!");
          $qry29 = mysqli_num_rows($sql29);
          $row29 = mysqli_fetch_array($sql29);
          if($qry29 > 0){
            $sql30 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp3_2', selesai_penilaian='$sp3_2' WHERE id='$row29[id]'")or die("Query 30 salah!");
          }else{
            $sql31 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$id_kontrak',3,'$mp3_2','$sp3_2','$time')")or die("Query 31 salah!");
          }
          $sql32 = mysqli_query($koneksi,"DELETE FROM tb_penilaian WHERE id_kontrak='$id_kontrak' && status_penilaian='4'");
        }else{
          $sql33 = mysqli_query($koneksi,"INSERT INTO tb_kontrak (id_karyawan,status_kontrak,banyak_penilaian,id_proses,mulai_kontrak,selesai_kontrak)VALUES('$id',2,'$banyak_penilaian',0,'$mulai_kontrak2','$selesai_kontrak2')");
          $sql34 = mysqli_query($koneksi,"SELECT a.id, a.id_kontrak, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='1' && b.status_kontrak='2'")or die("Query 33 Salah!");
          $row35 = mysqli_fetch_array($sql34);
          $sql36 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2', tgl_buat='$time' WHERE id='$row35[id]'")or die("Query 36 Salah!");
          $sql37 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$row35[id_kontrak]',2,'$mp2_2','$sp2_2','$time')")or die("Query 37 Salah!");
          $sql38 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$row35[id_kontrak]',3,'$mp3_2','$sp3_2','$time')")or die("Query 38 Salah!");
        }
        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }elseif ($status_karyawan == "4") {
      if ($banyak_penilaian == "1" && $status_karyawan == "4") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif ($banyak_penilaian == "2" && $status_karyawan == "4") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif ($banyak_penilaian == "3" && $status_karyawan == "4") {
        echo "<script>alert('Assessment Status tidak boleh lebih besar dari Sum Assessments!');history.go(-1)</script>";
      }elseif (empty($mp1)) {
        echo "<script>alert('Start Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($sp1)) {
        echo "<script>alert('Finish Assessment Period 1 harus di isi!');history.go(-1)</script>";
      }elseif (empty($mp2)) {
        echo "<script>alert('Start Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp2)) {
        echo "<script>alert('Finish Assessment Period 2 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($mp3)) {
        echo "<script>alert('Start Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp3)) {
        echo "<script>alert('Finish Assessment Period 3 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($mp4)) {
        echo "<script>alert('Start Assessment Period 4 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }elseif (empty($sp4)) {
        echo "<script>alert('Finish Assessment Period 4 harus di isi atau silahkan pilih Assessment Status yang sesuai!');history.go(-1)</script>";
      }else{
        if (!empty($row3['status_penilaian'])) {
          $sql39 = mysqli_query($koneksi,"UPDATE tb_kontrak SET id_proses=0, banyak_penilaian='$banyak_penilaian', mulai_kontrak='$mulai_kontrak2', selesai_kontrak='$selesai_kontrak2' WHERE id='$id_kontrak'")or die("Query 39 salah!");
          $sql40 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2' WHERE id='$id_penilaian[0]'")or die("Query 40 salah!");
          $sql41 = mysqli_query($koneksi,"SELECT a.id, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='2' && b.status_kontrak='2'")or die("Query 41 Salah!");
          $qry41 = mysqli_num_rows($sql41);
          $row41 = mysqli_fetch_array($sql41);
          if($qry41 > 0){
            $sql42 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp2_2', selesai_penilaian='$sp2_2' WHERE id='$row41[id]'")or die("Query 42 Salah!");
          }else{
            $sql43 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$id_kontrak',2,'$mp2_2','$sp2_2','$time')")or die("Query 43 salah!");
          }
          $sql44 = mysqli_query($koneksi,"SELECT a.id, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='3' && b.status_kontrak='2'")or die("Query 26 Salah!");
          $qry44 = mysqli_num_rows($sql44);
          $row44 = mysqli_fetch_array($sql44);
          if($qry44 > 0){
            $sql45 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp3_2', selesai_penilaian='$sp3_2' WHERE id='$row44[id]'")or die("Query 45 Salah!");
          }else{
            $sql46 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$id_kontrak',3,'$mp3_2','$sp3_2','$time')")or die("Query 46 salah!");
          }
          $sql47 = mysqli_query($koneksi,"SELECT a.id, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='4' && b.status_kontrak='2'")or die("Query 47 Salah!");
          $qry47 = mysqli_num_rows($sql47);
          $row47 = mysqli_fetch_array($sql47);
          if($qry47 > 0){
            $sql48 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp4_2', selesai_penilaian='$sp4_2' WHERE id='$row47[id]'")or die("Query 48 Salah!");
          }else{
            $sql49 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$id_kontrak',4,'$mp4_2','$sp4_2','$time')")or die("Query 49 salah!");
          }
        }else{
          $sql50 = mysqli_query($koneksi,"INSERT INTO tb_kontrak (id_karyawan,status_kontrak,banyak_penilaian,id_proses,mulai_kontrak,selesai_kontrak)VALUES('$id',2,'$banyak_penilaian',0,'$mulai_kontrak2','$selesai_kontrak2')")or die("Query 50 Salah!");
          $sql51 = mysqli_query($koneksi,"SELECT a.id, a.id_kontrak, b.status_kontrak FROM tb_penilaian AS a INNER JOIN tb_kontrak AS b ON a.id_kontrak=b.id WHERE a.id_karyawan='$id' && a.status_penilaian='1' && b.status_kontrak='2'")or die("Query 51 Salah!");
          $row51 = mysqli_fetch_array($sql51);
          $sql52 = mysqli_query($koneksi,"UPDATE tb_penilaian SET mulai_penilaian='$mp1_2', selesai_penilaian='$sp1_2', tgl_buat='$time' WHERE id='$row51[id]'")or die("Query 52 Salah!");
          $sql53 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$row51[id_kontrak]',2,'$mp2_2','$sp2_2','$time')")or die("Query 53 Salah!");
          $sql54 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$row51[id_kontrak]',3,'$mp3_2','$sp3_2','$time')")or die("Query 54 Salah!");
          $sql55 = mysqli_query($koneksi,"INSERT INTO tb_penilaian(id_karyawan,id_kontrak,status_penilaian,mulai_penilaian,selesai_penilaian,tgl_buat)VALUES('$id','$row51[id_kontrak]',4,'$mp4_2','$sp4_2','$time')")or die("Query 55 Salah!");
        }
        echo "<script>alert('Change Contract 2 telah berhasil diubah.');window.location='editass.php?nik=$nik'; </script>";
      }
    }
  }

  

  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <span><?php echo $_SESSION['adminlogin'];?></span>
            <i class="fas fa-user-alt"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div class="dropdown-divider"></div>
            <a href="setting.php" class="dropdown-item">
              <i class="fas fa-cog mr-2"></i>
              <span class="float-right text-muted text-sm">Setting</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="logout.php" class="dropdown-item">
              <i class="fas fa-sign-out-alt mr-2"></i>
              <span class="float-right text-muted text-sm">Logout</span>
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-olive elevation-4">
      <!-- Brand Logo -->
      <a href="index.php" class="brand-link navbar-light">
        <img src="gambar/logociputra.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <?php
            $cek_foto = $row['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
            ?>
          </div>
          <div class="info">
            <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="createass.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Contract 2</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item"><a href="viewass.php">View Assessment</a></li>
              <li class="breadcrumb-item"><a href="editass.php?nik<?php echo $row2['nik'];?>">Edit Assessment</a></li>
              <li class="breadcrumb-item active">Contract 2</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid col-8">
        <!-- /.row -->
        
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-olive">
          <div class="card-header">
            <h3 class="card-title">Change Contract 2</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form action="" method="post">
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label>NIK</label>
                    <input type="text" disabled class="form-control border-list-olive" value="<?php echo $row2['nik'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    <label>Assessment Status</label>
                    <select  name="status_karyawan" class="form-control select2" style="width: 100%;">
                      <option value="1" <?php if($qry3 > 0){if($row3['sp'] == 1) echo "selected";};?>>Assessment 1</option>
                      <option value="2" <?php if($qry3 > 0){if($row3['sp'] == 2) echo "selected";};?>>Assessment 2</option>
                      <option value="3" <?php if($qry3 > 0){if($row3['sp'] == 3) echo "selected";};?>>Assessment 3</option>
                      <option value="4" <?php if($qry3 > 0){if($row3['sp'] == 4) echo "selected";};?>>Assessment 4</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-12">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Contract</label>
                    <div class="input-group date" id="reservationdate1" data-target-input="nearest">
                      <input required name="mulai_kontrak" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate1" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($row3['status_penilaian'])){echo $mk2;}?>">
                      <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label>Finish Contract</label>
                    <div class="input-group date" id="reservationdate2" data-target-input="nearest">
                      <input required name="selesai_kontrak" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate2" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($row3['status_penilaian'])){echo $sk2;}?>">
                      <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label>Sum Assessments</label>
                    <select name="banyak_penilaian" class="form-control select2" style="width: 100%;">
                      <option value="1" <?php if($qry3 > 0)if($row3['banyak_penilaian'] == 1) echo "selected";?>>1</option>
                      <option value="2" <?php if($qry3 > 0)if($row3['banyak_penilaian'] == 2) echo "selected";?>>2</option>
                      <option value="3" <?php if($qry3 > 0)if($row3['banyak_penilaian'] == 3) echo "selected";?>>3</option>
                      <option value="4" <?php if($qry3 > 0)if($row3['banyak_penilaian'] == 4) echo "selected";?>>4</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 1</label>
                    <div class="input-group date" id="reservationdate3" data-target-input="nearest">
                      <input name="mp1" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate3" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($qry4 > 0)if($ubahmp != "00-00-0000")echo $ubahmp2;?>">
                      <div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 1</label>
                    <div class="input-group date" id="reservationdate4" data-target-input="nearest">
                      <input name="sp1" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate4" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if($qry4 > 0)if($ubahsp != "00-00-0000")echo $ubahsp2;?>">
                      <div class="input-group-append" data-target="#reservationdate4" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 2</label>
                    <div class="input-group date" id="reservationdate5" data-target-input="nearest">
                      <input name="mp2" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate5" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($mulai_penilaian[1]))echo date("d/m/Y",strtotime($mulai_penilaian[1]));?>">
                      <div class="input-group-append" data-target="#reservationdate5" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 2</label>
                    <div class="input-group date" id="reservationdate6" data-target-input="nearest">
                      <input name="sp2" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate6" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($selesai_penilaian[1]))echo date("d/m/Y",strtotime($selesai_penilaian[1]));?>">
                      <div class="input-group-append" data-target="#reservationdate6" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 3</label>
                    <div class="input-group date" id="reservationdate7" data-target-input="nearest">
                      <input name="mp3" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate7" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($mulai_penilaian[2]))echo date("d/m/Y",strtotime($mulai_penilaian[2]));?>">
                      <div class="input-group-append" data-target="#reservationdate7" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 3</label>
                    <div class="input-group date" id="reservationdate8" data-target-input="nearest">
                      <input name="sp3" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate8" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($selesai_penilaian[2]))echo date("d/m/Y",strtotime($selesai_penilaian[2]));?>">
                      <div class="input-group-append" data-target="#reservationdate8" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Start Assessment Period 4</label>
                    <div class="input-group date" id="reservationdate9" data-target-input="nearest">
                      <input name="mp4" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate9" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($mulai_penilaian[3]))echo date("d/m/Y",strtotime($mulai_penilaian[3]));?>">
                      <div class="input-group-append" data-target="#reservationdate9" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Finish Assessment Period 4</label>
                    <div class="input-group date" id="reservationdate10" data-target-input="nearest">
                      <input name="sp4" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate10" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php if(!empty($selesai_penilaian[3]))echo date("d/m/Y",strtotime($selesai_penilaian[3]));?>">
                      <div class="input-group-append" data-target="#reservationdate10" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <input type="hidden" name="id" value="<?php echo $id;?>">
              <button onclick="return confirm('Apakah User Id yang anda buat sudah benar?')" name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
              <a href="editass.php?nik=<?php echo $row2['nik'];?>" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp; </a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate1').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate2').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate3').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate4').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate5').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate6').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate7').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate8').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate9').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    $('#reservationdate10').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent : false
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
</body>
</html>
