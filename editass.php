<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS Manual -->
  <!-- <link rel="stylesheet" type="text/css" href="assets/css/style.css"> -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);


  $nik = $_GET['nik'];

  $sqlkaryawan = mysqli_query($koneksi,"SELECT * FROM editass_karyawan WHERE nik='$nik'")or die("Query Karyawan 1 Salah");
  $rowkaryawan= mysqli_fetch_array($sqlkaryawan);
  $tglmasuk = date("d-m-Y", strtotime($rowkaryawan['tgl_masuk']));
  $dk = $rowkaryawan['departemen_karyawan'];
  $p = $rowkaryawan['posisi_karyawan'];

  $sqluser1 = mysqli_query($koneksi,"SELECT id, nama, departemen FROM tb_user WHERE login_status=2 ORDER BY nama ASC");
  $sqluser2 = mysqli_query($koneksi,"SELECT id, nama, departemen FROM tb_user WHERE login_status=2 || login_status=0 ORDER BY nama ASC");

  $sqlstatus = mysqli_query($koneksi,"SELECT MAX(status_kontrak) AS status_kontrak FROM tb_kontrak WHERE id_karyawan='$rowkaryawan[id]'");
  $rowstatus = mysqli_fetch_array($sqlstatus);


  $sqlkon1 = mysqli_query($koneksi,"SELECT * FROM editass_kontrak WHERE nik='$nik' && status_kontrak=1")or die("Query kon1 Salah");
  $no1 = 1;
  $no_1 = 1;
  $hasil1 = mysqli_num_rows($sqlkon1);

  $sqlkon2 = mysqli_query($koneksi,"SELECT * FROM editass_kontrak WHERE nik='$nik' && status_kontrak=2")or die("Query kon1 Salah");
  $no2 = 1;
  $no_2 = 1;
  $hasil2 = mysqli_num_rows($sqlkon2);

  $sqlkon3 = mysqli_query($koneksi,"SELECT * FROM editass_kontrak WHERE nik='$nik' && status_kontrak=3")or die("Query kon1 Salah");
  $no3 = 1;
  $no_3 = 1;
  $hasil3 = mysqli_num_rows($sqlkon3);

  if (isset($_POST['submit'])) {
    // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    $id2 = $_POST['id'];
    $nama = trim($_POST['nama']);
    $nik2 = trim($_POST['nik']);
    $tgl_masuk = trim($_POST['tgl_masuk']);
    $departemen = trim($_POST['departemen']);
    $posisi = trim($_POST['posisi']);
    $golongan = trim($_POST['golongan']);
    $lokasi = trim($_POST['lokasi']);
    $user_proses = trim($_POST['user_proses']);
    $user_review = trim($_POST['user_review']);

    function ubahTanggal1($tgl_masuk){
      $pisah1 = explode('/',$tgl_masuk);
      $array1 = array($pisah1[2],$pisah1[1],$pisah1[0]);
      $satukan1 = implode('-',$array1);
      return $satukan1;
    }

    $tgl_masuk2 = ubahTanggal1($tgl_masuk);


    $cek = "SELECT nik FROM tb_karyawan WHERE nik='$nik'";
    $cekqry = mysqli_query($koneksi, $cek);
    $cekrow = mysqli_fetch_array($cekqry);

    if ($rowkaryawan['nama_karyawan'] == $nama) {
      if ($rowkaryawan['nik'] == $nik2) {
        if ($rowkaryawan['tgl_masuk'] == $tgl_masuk2) {
          if ($rowkaryawan['departemen_karyawan'] == $departemen) {
            if ($rowkaryawan['posisi_karyawan'] == $posisi) {
              if ($rowkaryawan['golongan'] == $golongan) {
                if ($rowkaryawan['lokasi'] == $lokasi) {
                  if($rowkaryawan['id_user1'] == $user_proses) {
                    if($rowkaryawan['id_user2'] == $user_review) {
                      echo "<script>alert('Tidak ada data yang berubah!');window.location='viewass.php'; </script>";
                    }
                  }
                }
              }
            }
          }
        }
      }
    }


    if (empty($nama) && empty($nik2) && empty($tgl_masuk2) && empty($departemen) && empty($posisi) && empty($golongan) && empty($lokasi) && empty($user_proses)&& empty($user_review)) {
      echo "<script>alert('Data masih kosong!');history.go(-1)</script>";
    }elseif (empty($nama)) {
      echo "<script>alert('Name Employment harus di isi!');history.go(-1)</script>";
    }elseif (empty($nik2)) {
      echo "<script>alert('NIK harus di isi!');history.go(-1)</script>";
    }elseif (empty($tgl_masuk)) {
      echo "<script>alert('Date Join harus di isi!');history.go(-1)</script>";
    }elseif (empty($departemen)) {
      echo "<script>alert('Departement harus di isi!');history.go(-1)</script>";
    }elseif (empty($posisi)) {
      echo "<script>alert('Position harus di isi!');history.go(-1)</script>";  
    }elseif (empty($golongan)) {
      echo "<script>alert('Golongan harus di isi!');history.go(-1)</script>";
    }elseif (empty($user_proses)) {
      echo "<script>alert('Name User Process harus di isi!');history.go(-1)</script>"; 
    }elseif (empty($user_review)) {
      echo "<script>alert('Name User Review harus di isi!');history.go(-1)</script>"; 
    }elseif (!preg_match("/^[a-zA-Z ]*$/", $nama)) {
      echo "<script>alert('Name Employment tidak boleh menganduk special char dan angka!');history.go(-1)</script>";
    }elseif (strlen($nama) >= 60) {
      echo "<script>alert('Panjang Name Employment tidak boleh 60 Character!');history.go(-1)</script>";
    }elseif (strlen($nik) <= 7 && strlen($nik) >= 11) {
      echo "<script>alert('Panjang NIK tidak boleh lebih kecil dari 8 character atau lebih besar 10 character!');history.go(-1)</script>";
    }elseif (strlen($golongan) >= 3) {
      echo "<script>alert('Panjang Golongan tidak boleh lebih besar dari 2!');history.go(-1)</script>";
    }else{
      $sqlupdate = "UPDATE tb_karyawan SET nik='$nik2', nama_karyawan='$nama', id_user1='$user_proses', id_user2='$user_review', tgl_masuk='$tgl_masuk2', lokasi='$lokasi', departemen_karyawan='$departemen', posisi_karyawan='$posisi', golongan='$golongan' WHERE id='$id2'";
      $hasilupdate = mysqli_query($koneksi, $sqlupdate);
      echo "<script>alert('Employment Assessment telah berhasil diubah.');window.location='viewass.php'; </script>";
    }
  }

  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['adminlogin']; ?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link navbar-light">
      <img src="gambar/logociputra.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
          $cek_foto = $row['foto'];
          $tempat_foto = 'foto/'.$row['foto']; 
          if ($cek_foto) {
            echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
          }else{
            echo "<img src='foto/blank.png'></a>";
          }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="createass.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link active">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Assessment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item"><a href="viewass.php">View Assessment</a></li>
              <li class="breadcrumb-item active">Edit Assessment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 1</h3>

                <div class="card-tools">
                  <a href="kontrak1.php?id_karyawan=<?php echo $rowkaryawan['id'];?>" type="button" class="btn btn-tool"><i class="fas fa-edit"></i></a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <?php if($hasil1 > 0){?>
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                  <?php };?>
                  <?php while ($rowkon1=mysqli_fetch_array($sqlkon1)) {
                    if ($rowkon1['status_kontrak'] == 1) {?>
                      <tbody>
                        <tr>
                          <td><?php echo $no1++ ;?></td>
                          <td><?php echo $rowkon1['nama_karyawan'];?></td>
                          <td><?php echo $rowkon1['nik'];?></td>
                          <td>Assessment <?php echo $rowkon1['status_penilaian'];?></td>
                          <td><?php if($rowkon1['mulai_penilaian'] != "0000-00-00") echo date("d-m-Y", strtotime($rowkon1['mulai_penilaian']));?></td>
                          <td><?php if($rowkon1['selesai_penilaian'] != "0000-00-00") echo date("d-m-Y", strtotime($rowkon1['selesai_penilaian']));?></td>
                          <td><center><?php echo $rowkon1['hasil'];?></center></td>
                          <td>
                            <div style="padding-left: 12px;" class="btn-group">
                              <a href="delete-penilaian.php?id=<?php echo $rowkon1['id'].".1.".$no_1++;?>" onclick="return confirm('Anda yakin ingin menghapus pada Contract 1 Penilaian <?php echo $rowkon1['status_penilaian'];?>?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                              <a href="<?php if($rowkon1['banyak_penilaian'] == $rowkon1['status_penilaian']){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $rowkon1['id'];?>" target='blank' class="btn btn-primary"><i class='fas fa-print'></i></a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    <?php };?>
                  <?php };?>
                </table>
                <b>*Lihat paling kanan untuk proses Delete/Print</b>
                <br>
                <b>*Lihat paling bawah untuk merubah data karyawan</b>
                <br>
                <b>*Icon Edit berada pojok kanan atas tabel untuk merubah tanggal penilaian</b>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>


        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 2</h3>

                <div class="card-tools">
                  <a href="kontrak2.php?id_karyawan=<?php echo $rowkaryawan['id'];?>" type="button" class="btn btn-tool"><i class="fas fa-edit"></i></a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <?php if($hasil2 > 0){?>
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                  <?php }?>
                  <?php while ($rowkon2=mysqli_fetch_array($sqlkon2)) {
                    if ($rowkon2['status_kontrak'] == 2) {?>
                      <tbody>
                        <tr>
                          <td><?php echo $no2++ ;?></td>
                          <td><?php echo $rowkon2['nama_karyawan'];?></td>
                          <td><?php echo $rowkon2['nik'];?></td>
                          <td>Assessment <?php echo $rowkon2['status_penilaian'];?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon2['mulai_penilaian']));?></td>
                          <td><?php echo date("d-m-Y", strtotime($rowkon2['selesai_penilaian']));?></td>
                          <td><center><?php echo $rowkon2['hasil'];?></center></td>
                          <td>
                            <div style="padding-left: 12px;" class="btn-group">
                              <a href="delete-penilaian.php?id=<?php echo $rowkon2['id'].".2.".$no_2++;?>" onclick="return confirm('Anda yakin ingin menghapus pada Contract 2 Penilaian <?php echo $rowkon2['status_penilaian'];?>?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                              <a href="<?php if($rowkon2['banyak_penilaian'] == $rowkon2['status_penilaian']){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $rowkon2['id'];?>" target='blank' class="btn btn-primary"><i class='fas fa-print'></i></a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    <?php };?>
                  <?php };?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="card card-olive">
              <div class="card-header">
                <h3 class="card-title">Contract 3</h3>

                <div class="card-tools">
                  <a href="kontrak3.php?id_karyawan=<?php echo $rowkaryawan['id'];?>" type="button" class="btn btn-tool"><i class="fas fa-edit"></i></a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-bordered table-hover text-nowrap">
                  <?php if($hasil3 > 0){?>
                    <thead>
                      <tr>
                        <th style="padding-left: 12px;" width="6%"><center>No</center></th>
                        <th><center>Name</center></th>
                        <th><center>NIK</center></th>
                        <th><center>Assessment Status</center></th>
                        <th><center>Start Assessment</center></th>
                        <th><center>Finish Assessment</center></th>
                        <th><center>Result User</center></th>
                        <th style="padding-right: 12px;" width="10%"><center>Action</center></th>
                      </tr>
                    </thead>
                  <?php }?>
                  <tbody>
                    <?php if($hasil3 > 0){;
                      while ($rowkon3=mysqli_fetch_array($sqlkon3)) {
                        if ($rowkon3['status_kontrak'] == 3) {?>
                          <tr>
                            <td><?php echo $no3++ ;?></td>
                            <td><?php echo $rowkon3['nama_karyawan'];?></td>
                            <td><?php echo $rowkon3['nik'];?></td>
                            <td>Assessment <?php echo $rowkon3['status_penilaian'];?></td>
                            <td><?php echo date("d-m-Y", strtotime($rowkon3['mulai_penilaian']));?></td>
                            <td><?php echo date("d-m-Y", strtotime($rowkon3['selesai_penilaian']));?></td>
                            <td><center><?php echo $rowkon3['hasil'];?></center></td>
                            <td>
                              <div style="padding-left: 12px;" class="btn-group">
                                <a href="delete-penilaian.php?id=<?php echo $rowkon3['id'].".3.".$no_3++;?>" onclick="return confirm('Anda yakin ingin menghapus pada Contract 3 Penilaian <?php echo $rowkon3['status_penilaian'];?>?')" class="btn btn-danger"><i class='fas fa-trash-alt'></i></a>
                                <a href="<?php if($rowkon3['banyak_penilaian'] == $rowkon3['status_penilaian']){echo "printass2";}else{echo "printass";};?>.php?id=<?php echo $rowkon3['id'];?>" target='blank' class="btn btn-primary"><i class='fas fa-print'></i></a>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      <?php };?>
                    <?php };?>
                  <?php };?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <form action="" method="post">
          <!-- SELECT2 EXAMPLE -->
          <div class="card card-olive">
            <div class="card-header">
              <h3 class="card-title">Create New Employee Assessment</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Name Employee</label>
                    <input  name="nama" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['nama_karyawan'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>NIK</label>
                    <input  name="nik" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['nik'];?>">
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Date Join</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input  name="tgl_masuk" type="text" class="form-control border-list-olive datetimepicker-input" data-target="#reservationdate" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask value="<?php echo $tglmasuk;?>">
                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Employment Status</label>
                    <selects class="form-control select2" style="width: 100%;">
                      <option disabled selected="selected">Kontrak <?php echo $rowstatus['status_kontrak'];?></option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Departement</label>
                    <select name="departemen" class="form-control select2" style="width: 100%;">
                      <option selected="selected"></option>
                      <option value="BD" <?php if($dk == "DB") echo "selected";?>>BD</option>
                      <option value="CONS" <?php if($dk == "CONS") echo "selected";?>>CONS</option>
                      <option value="DGMKT" <?php if($dk == "DGMKT") echo "selected";?>>DGMKT</option>
                      <option value="EM" <?php if($dk == "EM") echo "selected";?>>EM</option>
                      <option value="FA" <?php if($dk == "FA") echo "selected";?>>FA</option>
                      <option value="HCGA" <?php if($dk == "HCGA") echo "selected";?>>HCGA</option>
                      <option value="LEGP" <?php if($dk == "LEGP") echo "selected";?>>LEGP</option>
                      <option value="LND" <?php if($dk == "LND") echo "selected";?>>LND</option>
                      <option value="MCOM" <?php if($dk == "MCOM") echo "selected";?>>MCOM</option>
                      <option value="MIS" <?php if($dk == "MIS") echo "selected";?>>MIS</option>
                      <option value="MKTDA" <?php if($dk == "MKTDA") echo "selected";?>>MKTDA</option>
                      <option value="PDG" <?php if($dk == "PDG") echo "selected";?>>PDG</option>
                      <option value="QS" <?php if($dk == "QS") echo "selected";?>>QS</option>
                      <option value="SALES" <?php if($dk == "SALES") echo "selected";?>>SALES</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Position</label>
                    <select name="posisi" class="form-control select2 border-list-olive" style="width: 100%;">
                      <option selected="selected"></option>
                      <option value="ACCOUNTING ADMINISTRATOR" <?php if($p == "ACCOUNTING ADMINISTRATOR") echo "selected";?>>ACCOUNTING ADMINISTRATOR</option>
                      <option value="ACCOUNTING SUPERVISOR" <?php if($p == "ACCOUNTING SUPERVISOR") echo "selected";?>>ACCOUNTING SUPERVISOR</option>
                      <option value="ADMIN ACCONTING" <?php if($p == "ADMIN ACCOUNTING") echo "selected";?>>ADMIN ACCONTING</option>
                      <option value="ADMIN COLLECTION" <?php if($p == "ADMIN COLLECTION") echo "selected";?>>ADMIN COLLECTION</option>
                      <option value="ADMIN PURNA JUAL" <?php if($p == "ADMIN PURNA JUAL") echo "selected";?>>ADMIN PURNA JUAL</option>
                      <option value="AFTER SALES ADMINISTATION" <?php if($p == "AFTER SALES ADMINISTATION") echo "selected";?>>AFTER SALES ADMINISTATION</option>
                      <option value="AFTER SALES SUPERVISOR" <?php if($p == "AFTER SALES SUPERVISOR") echo "selected";?>>AFTER SALES SUPERVISOR</option>
                      <option value="ARCHITECT" <?php if($p == "ARCHITECT") echo "selected";?>>ARCHITECT</option>
                      <option value="ASSISTANT ACCOUNTING MANAGER" <?php if($p == "ASSISTANT ACCOUNTING MANAGER") echo "selected";?>>ASSISTANT ACCOUNTING MANAGER</option>
                      <option value="ASSISTANT COLLECTION MANAGER" <?php if($p == "ASSISTANT COLLECTION MANAGER") echo "selected";?>>ASSISTANT COLLECTION MANAGER</option>
                      <option value="ASSISTANT ESTATE MANAGEMENT MANAGER" <?php if($p == "ASSISTANT ESTATE MANAGEMENT MANAGER") echo "selected";?>>ASSISTANT ESTATE MANAGEMENT MANAGER</option>
                      <option value="ASSISTANT QUANTITY SURVEYOR MANAGER" <?php if($p == "ASSISTANT QUANTITY SURVEYOR MANAGER") echo "selected";?>>ASSISTANT QUANTITY SURVEYOR MANAGER</option>
                      <option value="ASSOCIATE DIRECTOR" <?php if($p == "ASSOCIATE DIRECTOR") echo "selected";?>>ASSOCIATE DIRECTOR</option>
                      <option value="ASSOCIATE DIRECTOR 1" <?php if($p == "ASSOCIATE DIRECTOR 1") echo "selected";?>>ASSOCIATE DIRECTOR 1</option>
                      <option value="BUILDING COORDINATOR" <?php if($p == "BUILDING COORDINATOR") echo "selected";?>>BUILDING COORDINATOR</option>
                      <option value="BUILDING INSPECTOR" <?php if($p == "BUILDING INSPECTOR") echo "selected";?>>BUILDING INSPECTOR</option>
                      <option value="BUILDING SITE MANAGER" <?php if($p == "BUILDING SITE MANAGER") echo "selected";?>>BUILDING SITE MANAGER</option>
                      <option value="BUSINESS DEVELOPMENT OFFICER" <?php if($p == "BUSINESS DEVELOPMENT OFFICER") echo "selected";?>>BUSINESS DEVELOPMENT OFFICER</option>
                      <option value="CASHIRE" <?php if($p == "CASHIRE") echo "selected";?>>CASHIRE</option>
                      <option value="COLLECTION ADMINISTRATOR" <?php if($p == "COLLECTION ADMINISTRATOR") echo "selected";?>>COLLECTION ADMINISTRATOR</option>
                      <option value="COLLECTION OFFICE" <?php if($p == "COLLECTION OFFICE") echo "selected";?>>COLLECTION OFFICE</option>
                      <option value="COMMERCIAL AREA INSPECTOR" <?php if($p == "COMMERCIAL AREA INSPECTOR") echo "selected";?>>COMMERCIAL AREA INSPECTOR</option>
                      <option value="CONSTRUCTION ADMINISTRATOR" <?php if($p == "CONSTRUCTION ADMINISTRATOR") echo "selected";?>>CONSTRUCTION ADMINISTRATOR</option>
                      <option value="CONSTRUCTION MANAGER" <?php if($p == "CONSTRUCTION MANAGER") echo "selected";?>>CONSTRUCTION MANAGER</option>
                      <option value="CUSTOMER SERVICE" <?php if($p == "CUSTOMER SERVICE") echo "selected";?>>CUSTOMER SERVICE</option>
                      <option value="DESIGN GRAPHIC" <?php if($p == "DESIGN GRAPHIC") echo "selected";?>>DESIGN GRAPHIC</option>
                      <option value="DIGITAL MARKETING MANAGER" <?php if($p == "DIGITAL MARKETING MANAGER") echo "selected";?>>DIGITAL MARKETING MANAGER</option>
                      <option value="DIGITAL MARKETING OFFICER" <?php if($p == "DIGITAL MARKETING OFFICER") echo "selected";?>>DIGITAL MARKETING OFFICER</option>
                      <option value="DIRECTOR" <?php if($p == "DIRECTOR") echo "selected";?>>DIRECTOR</option>
                      <option value="DRAFTER" <?php if($p == "DRAFTER") echo "selected";?>>DRAFTER</option>
                      <option value="DRIVER" <?php if($p == "DRIVER") echo "selected";?>>DRIVER</option>
                      <option value="ENVIRONMENT MAINTENANCE INSPECTOR" <?php if($p == "ENVIRONMENT MAINTENANCE INSPECTOR") echo "selected";?>>ENVIRONMENT MAINTENANCE INSPECTOR</option>
                      <option value="ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR" <?php if($p == "ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR") echo "selected";?>>ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR</option>
                      <option value="ESTATE COORDINATOR" <?php if($p == "ESTATE COORDINATOR") echo "selected";?>>ESTATE COORDINATOR</option>
                      <option value="ESTATE MANAGEMENT ADMINISTRATOR" <?php if($p == "ESTATE MANAGEMENT ADMINISTRATOR") echo "selected";?>>ESTATE MANAGEMENT ADMINISTRATOR</option>
                      <option value="ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD" <?php if($p == "ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD") echo "selected";?>>ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD</option>
                      <option value="ESTATE MANAGEMENT DEPUTY MANAGER" <?php if($p == "ESTATE MANAGEMENT DEPUTY MANAGER") echo "selected";?>>ESTATE MANAGEMENT DEPUTY MANAGER</option>
                      <option value="FINANCE & ACCOUNTING DEPUTY MANAGER" <?php if($p == "FINANCE & ACCOUNTING DEPUTY MANAGER") echo "selected";?>>FINANCE & ACCOUNTING DEPUTY MANAGER</option>
                      <option value="FINANCE & ACCOUNTING GENERAL MANAGER" <?php if($p == "FINANCE & ACCOUNTING GENERAL MANAGER") echo "selected";?>>FINANCE & ACCOUNTING GENERAL MANAGER</option>
                      <option value="FINANCE & ACCOUNTING MANAGER" <?php if($p == "FINANCE & ACCOUNTING MANAGER") echo "selected";?>>FINANCE & ACCOUNTING MANAGER</option>
                      <option value="FINANCE ADMINISTRATOR" <?php if($p == "FINANCE ADMINISTRATOR") echo "selected";?>>FINANCE ADMINISTRATOR</option>
                      <option value="FINANCE, ACCOUNTING & TAX DEPUTY MANAGER" <?php if($p == "FINANCE, ACCOUNTING & TAX DEPUTY MANAGER") echo "selected";?>>FINANCE, ACCOUNTING & TAX DEPUTY MANAGER</option>
                      <option value="GENERAL AFFAIR SECTION HEAD" <?php if($p == "GENERAL AFFAIR SECTION HEAD") echo "selected";?>>GENERAL AFFAIR SECTION HEAD</option>
                      <option value="GM OPERATIONAL" <?php if($p == "GM OPERATIONAL") echo "selected";?>>GM OPERATIONAL</option>
                      <option value="HC & GA DEPUTY GM" <?php if($p == "HC & GA DEPUTY GM") echo "selected";?>>HC & GA DEPUTY GM</option>
                      <option value="HC & GA MANAGER" <?php if($p == "HC & GA MANAGER") echo "selected";?>>HC & GA MANAGER</option>
                      <option value="HC ADMINISTRATOR" <?php if($p == "HC ADMINISTRATOR") echo "selected";?>>HC ADMINISTRATOR</option>
                      <option value="HC OFFICER" <?php if($p == "HC OFFICER") echo "selected";?>>HC OFFICER</option>
                      <option value="HSE INSPECTOR" <?php if($p == "HSE INSPECTOR") echo "selected";?>>HSE INSPECTOR</option>
                      <option value="HSE OFFICER" <?php if($p == "HSE OFFICER") echo "selected";?>>HSE OFFICER</option>
                      <option value="INFRASTRUCTURE INSPECTOR" <?php if($p == "INFRASTRUCTURE INSPECTOR") echo "selected";?>>INFRASTRUCTURE INSPECTOR</option>
                      <option value="INFRASTRUCTURE SITE MANAGER" <?php if($p == "INFRASTRUCTURE SITE MANAGER") echo "selected";?>>INFRASTRUCTURE SITE MANAGER</option>
                      <option value="INVOICE ADMINISTRATION" <?php if($p == "INVOICE ADMINISTRATION") echo "selected";?>>INVOICE ADMINISTRATION</option>
                      <option value="LAND ADMINISTRATOR"<?php if($p == "LAND ADMINISTRATOR") echo "selected";?>>LAND ADMINISTRATOR</option>
                      <option value="LAND MANAGER" <?php if($p == "LAND MANAGER") echo "selected";?>>LAND MANAGER</option>
                      <option value="LAND OFFICER" <?php if($p == "LAND OFFICER") echo "selected";?>>LAND OFFICER</option>
                      <option value="LAND SECTION HEAD" <?php if($p == "LAND SECTION HEAD") echo "selected";?>>LAND SECTION HEAD</option>
                      <option value="LAND SURVEYOR" <?php if($p == "LAND SURVEYOR") echo "selected";?>>LAND SURVEYOR</option>
                      <option value="LANDSCAPE DESIGN" <?php if($p == "LANDSCAPE DESIGN") echo "selected";?>>LANDSCAPE DESIGN</option>
                      <option value="LANDSCAPE INSPECTOR" <?php if($p == "LANDSCAPE INSPECTOR") echo "selected";?>>LANDSCAPE INSPECTOR</option>
                      <option value="LANDSCAPE SECTION HEAD" <?php if($p == "LANDSCAPE SECTION HEAD") echo "selected";?>>LANDSCAPE SECTION HEAD</option>
                      <option value="LEGAL & PERMIT MANAGER" <?php if($p == "LEGAL & PERMIT MANAGER") echo "selected";?>>LEGAL & PERMIT MANAGER</option>
                      <option value="LEGAL ADMINISTRATOR" <?php if($p == "LEGAL ADMINISTRATOR") echo "selected";?>>LEGAL ADMINISTRATOR</option>
                      <option value="LEGAL OFFICER" <?php if($p == "LEGAL OFFICER") echo "selected";?>>LEGAL OFFICER</option>
                      <option value="LEGAL SECTION HEAD" <?php if($p == "LEGAL SECTION HEAD") echo "selected";?>>LEGAL SECTION HEAD</option>
                      <option value="LIFE GUARD" <?php if($p == "LIFE GUARD") echo "selected";?>>LIFE GUARD</option>
                      <option value="MANAGEMENT INFORMATION SYSTEMS MANAGER" <?php if($p == "MANAGEMENT INFORMATION SYSTEMS MANAGER") echo "selected";?>>MANAGEMENT INFORMATION SYSTEMS MANAGER</option>
                      <option value="MARKETING MANAGER" <?php if($p == "MARKETING MANAGER") echo "selected";?>>MARKETING MANAGER</option>
                      <option value="MECHANICAL ELECTRICAL INSPECTOR" <?php if($p == "MECHANICAL ELECTRICAL INSPECTOR") echo "selected";?>>MECHANICAL ELECTRICAL INSPECTOR</option>
                      <option value="MECHANICAL ELECTRICAL SECTION HEAD" <?php if($p == "MECHANICAL ELECTRICAL SECTION HEAD") echo "selected";?>>MECHANICAL ELECTRICAL SECTION HEAD</option>
                      <option value="MECHANICAL ELECTRICAL SITE MANAGER" <?php if($p == "MECHANICAL ELECTRICAL SITE MANAGER") echo "selected";?>>MECHANICAL ELECTRICAL SITE MANAGER</option>
                      <option value="MESSENGER" <?php if($p == "MESSENGER") echo "selected";?>>MESSENGER</option>
                      <option value="MIS COORDINATOR" <?php if($p == "MIS COORDINATOR") echo "selected";?>>MIS COORDINATOR</option>
                      <option value="MIS OFFICER" <?php if($p == "MIS OFFICER") echo "selected";?>>MIS OFFICER</option>
                      <option value="OFFICE BOY" <?php if($p == "OFFICE BOY") echo "selected";?>>OFFICE BOY</option>
                      <option value="OPERATOR GENERAL MANAGER" <?php if($p == "OPERATOR GENERAL MANAGER") echo "selected";?>>OPERATOR GENERAL MANAGER</option>
                      <option value="OPERATOR HELPER" <?php if($p == "OPERATOR HELPER") echo "selected";?>>OPERATOR HELPER</option>
                      <option value="PAYROLL OFFICER" <?php if($p == "PAYROLL OFFICER") echo "selected";?>>PAYROLL OFFICER</option>
                      <option value="PENGAWAS BANGUNAN" <?php if($p == "PENGAWAS BANGUNAN") echo "selected";?>>PENGAWAS BANGUNAN</option>
                      <option value="PENGAWAS ME" <?php if($p == "PENGAWAS ME") echo "selected";?>>PENGAWAS ME</option>
                      <option value="PERMIT ADMINISTRATOR" <?php if($p == "PERMIT ADMINISTRATOR") echo "selected";?>>PERMIT ADMINISTRATOR</option>
                      <option value="PERMIT OFFICER" <?php if($p == "PERMIT OFFICER") echo "selected";?>>PERMIT OFFICER</option>
                      <option value="PLANNING & DESIGN GENERAL MANAGER" <?php if($p == "PLANNING & DESIGN GENERAL MANAGER") echo "selected";?>>PLANNING & DESIGN GENERAL MANAGER</option>
                      <option value="PLANNING & DESIGN MANAGER" <?php if($p == "PLANNING & DESIGN MANAGER") echo "selected";?>>PLANNING & DESIGN MANAGER</option>
                      <option value="PROMOTION ADMINISTRATOR" <?php if($p == "PROMOTION ADMINISTRATOR") echo "selected";?>>PROMOTION ADMINISTRATOR</option>
                      <option value="PROMOTION SUPERVISOR" <?php if($p == "PROMOTION SUPERVISOR") echo "selected";?>>PROMOTION SUPERVISOR</option>
                      <option value="PROMOTION SUPPORT" <?php if($p == "PROMOTION SUPPORT") echo "selected";?>>PROMOTION SUPPORT</option>
                      <option value="PURCHASING ADMINISTRATOR" <?php if($p == "PURCHASING ADMINISTRATOR") echo "selected";?>>PURCHASING ADMINISTRATOR</option>
                      <option value="QUALITY CONTROL" <?php if($p == "QUALITY CONTROL") echo "selected";?>>QUALITY CONTROL</option>
                      <option value="QUANTITY SURVEYOR ADMINISTRATOR" <?php if($p == "QUANTITY SURVEYOR ADMINISTRATOR") echo "selected";?>>QUANTITY SURVEYOR ADMINISTRATOR</option>
                      <option value="QUANTITY SURVEYOR ANALYST" <?php if($p == "QUANTITY SURVEYOR ANALYST") echo "selected";?>>QUANTITY SURVEYOR ANALYST</option>
                      <option value="QUANTITY SURVEYOR MANAGER" <?php if($p == "QUANTITY SURVEYOR MANAGER") echo "selected";?>>QUANTITY SURVEYOR MANAGER</option>
                      <option value="QUANTITY SURVEYOR OFFICER" <?php if($p == "QUANTITY SURVEYOR OFFICER") echo "selected";?>>QUANTITY SURVEYOR OFFICER</option>
                      <option value="SALES EXECUTIVE" <?php if($p == "SALES EXECUTIVE") echo "selected";?>>SALES EXECUTIVE</option>
                      <option value="SALES EXECUTIVE SUPERVISOR" <?php if($p == "SALES EXECUTIVE SUPERVISOR") echo "selected";?>>SALES EXECUTIVE SUPERVISOR</option>
                      <option value="SECURITY SECTION HEAD" <?php if($p == "SECURITY SECTION HEAD") echo "selected";?>>SECURITY SECTION HEAD</option>
                      <option value="SPORTCLUB, WW & WOW SECTION HEAD" <?php if($p == "SPORTCLUB, WW & WOW SECTION HEAD") echo "selected";?>>SPORTCLUB, WW & WOW SECTION HEAD</option>
                      <option value="SURVEYOR" <?php if($p == "SURVEYOR") echo "selected";?>>SURVEYOR</option>
                      <option value="TAX ADMINISTRATOR" <?php if($p == "TAX ADMINISTRATOR") echo "selected";?>>TAX ADMINISTRATOR</option>
                      <option value="TRAINER" <?php if($p == "TRAINER") echo "selected";?>>TRAINER</option>
                      <option value="WEB DEVELOPER" <?php if($p == "WEB DEVELOPER") echo "selected";?>>WEB DEVELOPER</option>
                      <option value="WTP OPERATOR" <?php if($p == "WTP OPERATOR") echo "selected";?>>WTP OPERATOR</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Golongan</label>
                    <input  name="golongan" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['golongan'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Location Employment</label>
                    <input  name="lokasi" type="text" class="form-control border-list-olive" value="<?php echo $rowkaryawan['lokasi'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Name User Process</label>
                    <select  name="user_proses" class="form-control select2" style="width: 100%;">
                      <?php while($rowuser1=mysqli_fetch_array($sqluser1)) {
                        if ($rowkaryawan['id_user1'] == $rowuser1['id']) {?>
                          <option selected="selected" value="<?php echo $rowkaryawan['id_user1'];?>"><?php echo "$rowkaryawan[nama]"." - "."$rowkaryawan[departemen]";?></option>
                        <?php }else{ ?>
                          <option value="<?php echo $rowuser1['id'];?>"><?php echo "$rowuser1[nama]"." - "."$rowuser1[departemen]";?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Name User Review</label>
                    <select name="user_review" class="form-control select2" style="width: 100%;">
                      <?php while($rowuser2=mysqli_fetch_array($sqluser2)) {
                        if ($rowkaryawan['id_user2'] == $rowuser2['id']){?>
                          <option selected="selected" value="<?php echo $rowkaryawan['id_user2'];?>"><?php echo "$rowkaryawan[nama2]"." - "."$rowkaryawan[departemen2]";?></option>
                        <?php }else{ ?>
                          <option value="<?php echo $rowuser2['id'];?>"><?php echo "$rowuser2[nama]"." - "."$rowuser2[departemen]";?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <b>*Employment Status silahkan ubah di tabel Contract di atas</b>
                <!-- /.col -->
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <input required type="hidden" name="id" value="<?php echo $rowkaryawan['id']; ?>">
              <button onclick="return confirm('Apakah User Id yang anda ubah sudah benar?')" name="submit" type="submit" class="btn btn-primary float-right">Submit</button>
              <a href="viewass.php" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp; </a>
            </div>
          </div>
          <!-- /.card -->
        </form>

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: false
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
</body>
</html>