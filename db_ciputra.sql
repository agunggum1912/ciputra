-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Sep 2020 pada 15.38
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ciputra`
--

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `assproses`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `assproses` (
`id_penilaian` int(10)
,`status_penilaian` int(1)
,`hasil` varchar(1)
,`id_kontrak` int(10)
,`status_kontrak` int(1)
,`banyak_penilaian` int(1)
,`nama_karyawan` varchar(60)
,`nik` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `assuser`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `assuser` (
`id` int(10)
,`id_user1` int(10)
,`nama_karyawan` varchar(60)
,`nik` varchar(10)
,`id_kontrak` int(10)
,`status_kontrak` int(1)
,`banyak_penilaian` int(1)
,`id_penilaian` int(10)
,`status_penilaian` int(1)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `editass_karyawan`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `editass_karyawan` (
`id` int(10)
,`nik` varchar(10)
,`nama_karyawan` varchar(60)
,`tgl_masuk` date
,`lokasi` varchar(60)
,`departemen_karyawan` varchar(30)
,`posisi_karyawan` varchar(50)
,`golongan` varchar(2)
,`id_user1` int(10)
,`id_user2` int(10)
,`nama` varchar(60)
,`departemen` varchar(5)
,`nama2` varchar(60)
,`departemen2` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `editass_kontrak`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `editass_kontrak` (
`id` int(10)
,`mulai_penilaian` date
,`selesai_penilaian` date
,`hasil` varchar(1)
,`id_kontrak` int(10)
,`status_penilaian` int(1)
,`nama_karyawan` varchar(60)
,`nik` varchar(10)
,`tgl_masuk` date
,`status_kontrak` int(1)
,`banyak_penilaian` int(1)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_reset_temp`
--

CREATE TABLE `password_reset_temp` (
  `email` varchar(250) NOT NULL,
  `key` varchar(250) NOT NULL,
  `expDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `printass`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `printass` (
`id_penilaian` int(10)
,`id` int(10)
,`id_karyawan` int(10)
,`id_kontrak` int(10)
,`status_penilaian` int(1)
,`mulai_penilaian` date
,`selesai_penilaian` date
,`tgl_buat` datetime
,`jawaban1` int(1)
,`jawaban2` int(1)
,`jawaban3` int(1)
,`jawaban4` int(1)
,`jawaban5` int(1)
,`jawaban6` int(1)
,`jawaban7` int(1)
,`jawaban8` int(1)
,`jawaban9` int(1)
,`jawaban10` int(1)
,`jawaban11` int(1)
,`ket1` varchar(1200)
,`ket2` varchar(1200)
,`ket3` varchar(1200)
,`hasil` varchar(1)
,`rekomendasi` varchar(70)
,`tgl_penilaian` datetime
,`mulai_kontrak` date
,`selesai_kontrak` date
,`banyak_penilaian` int(1)
,`status_kontrak` int(1)
,`nik` varchar(10)
,`nama_karyawan` varchar(60)
,`tgl_masuk` date
,`lokasi` varchar(60)
,`departemen_karyawan` varchar(30)
,`posisi_karyawan` varchar(50)
,`golongan` varchar(2)
,`tgl_buat_karyawan` timestamp
,`nama` varchar(60)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `id` int(10) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `nama_karyawan` varchar(60) NOT NULL,
  `id_user1` int(10) NOT NULL,
  `id_user2` int(10) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `lokasi` varchar(60) NOT NULL,
  `departemen_karyawan` varchar(30) NOT NULL,
  `posisi_karyawan` varchar(50) NOT NULL,
  `golongan` varchar(2) NOT NULL,
  `tgl_buat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`id`, `nik`, `nama_karyawan`, `id_user1`, `id_user2`, `tgl_masuk`, `lokasi`, `departemen_karyawan`, `posisi_karyawan`, `golongan`, `tgl_buat`) VALUES
(5, 'A1-1111', 'Thor', 12, 12, '2020-09-21', 'Ciputra', 'FA', 'ACCOUNTING ADMINISTRATOR', '1A', '2020-09-24 16:25:17'),
(6, 'A2-2222', 'Gumelar Agung', 14, 11, '2020-09-24', 'Citra Raya', 'HCGA', 'ADMIN ACCONTING', '1A', '2020-09-26 19:13:01'),
(7, 'A3-3333', 'Abdul', 14, 1, '2020-09-24', 'Tigaraksa', 'EM', 'ADMIN COLLECTION', '1B', '2020-09-24 16:23:35');

--
-- Trigger `tb_karyawan`
--
DELIMITER $$
CREATE TRIGGER `after_insert_karyawan` AFTER INSERT ON `tb_karyawan` FOR EACH ROW BEGIN
	INSERT INTO tb_kontrak (id_karyawan,id_proses) VALUES (NEW.id,1);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kontrak`
--

CREATE TABLE `tb_kontrak` (
  `id` int(10) NOT NULL,
  `id_karyawan` int(10) NOT NULL,
  `status_kontrak` int(1) NOT NULL,
  `banyak_penilaian` int(1) NOT NULL,
  `id_proses` int(1) NOT NULL,
  `mulai_kontrak` date NOT NULL,
  `selesai_kontrak` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kontrak`
--

INSERT INTO `tb_kontrak` (`id`, `id_karyawan`, `status_kontrak`, `banyak_penilaian`, `id_proses`, `mulai_kontrak`, `selesai_kontrak`) VALUES
(9, 5, 1, 4, 1, '2020-09-21', '2021-09-21'),
(27, 5, 2, 4, 1, '2021-09-24', '2022-09-24'),
(28, 5, 3, 4, 0, '2022-09-24', '2023-09-24'),
(29, 6, 1, 4, 1, '2020-09-24', '2021-09-24'),
(30, 6, 2, 4, 0, '2021-09-24', '2022-09-24'),
(31, 6, 3, 2, 0, '2022-09-24', '2023-09-24'),
(32, 7, 1, 2, 0, '2020-09-24', '2021-09-24');

--
-- Trigger `tb_kontrak`
--
DELIMITER $$
CREATE TRIGGER `after_insert_kontrak` AFTER INSERT ON `tb_kontrak` FOR EACH ROW BEGIN
	INSERT INTO tb_penilaian (id_karyawan,id_kontrak,status_penilaian) VALUES (NEW.id_karyawan,NEW.id,1);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_penilaian`
--

CREATE TABLE `tb_penilaian` (
  `id` int(10) NOT NULL,
  `id_karyawan` int(10) NOT NULL,
  `id_kontrak` int(10) NOT NULL,
  `status_penilaian` int(1) NOT NULL,
  `mulai_penilaian` date NOT NULL,
  `selesai_penilaian` date NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `jawaban1` int(1) NOT NULL,
  `jawaban2` int(1) NOT NULL,
  `jawaban3` int(1) NOT NULL,
  `jawaban4` int(1) NOT NULL,
  `jawaban5` int(1) NOT NULL,
  `jawaban6` int(1) NOT NULL,
  `jawaban7` int(1) NOT NULL,
  `jawaban8` int(1) NOT NULL,
  `jawaban9` int(1) NOT NULL,
  `jawaban10` int(1) NOT NULL,
  `jawaban11` int(1) NOT NULL,
  `ket1` varchar(1200) NOT NULL,
  `ket2` varchar(1200) NOT NULL,
  `ket3` varchar(1200) NOT NULL,
  `hasil` varchar(1) NOT NULL,
  `rekomendasi` varchar(70) NOT NULL,
  `tgl_penilaian` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_penilaian`
--

INSERT INTO `tb_penilaian` (`id`, `id_karyawan`, `id_kontrak`, `status_penilaian`, `mulai_penilaian`, `selesai_penilaian`, `tgl_buat`, `jawaban1`, `jawaban2`, `jawaban3`, `jawaban4`, `jawaban5`, `jawaban6`, `jawaban7`, `jawaban8`, `jawaban9`, `jawaban10`, `jawaban11`, `ket1`, `ket2`, `ket3`, `hasil`, `rekomendasi`, `tgl_penilaian`) VALUES
(22, 5, 9, 1, '2020-09-24', '2020-12-24', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(111, 5, 27, 1, '2021-09-24', '2021-12-24', '2020-09-24 20:50:06', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(112, 5, 27, 2, '2021-12-24', '2022-03-24', '2020-09-24 20:50:06', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(114, 5, 28, 1, '2022-09-24', '2022-12-24', '2020-09-24 21:02:37', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(115, 5, 28, 2, '2022-12-24', '2023-03-24', '2020-09-24 21:02:37', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(116, 6, 29, 1, '2020-09-24', '2020-12-24', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(117, 6, 29, 2, '2020-12-24', '2021-03-24', '2020-09-24 22:52:50', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(118, 6, 29, 3, '2021-03-24', '2021-06-24', '2020-09-24 22:52:50', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(119, 6, 30, 1, '2020-09-24', '2020-12-24', '2020-09-24 22:53:59', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 'A', '', '0000-00-00 00:00:00'),
(120, 6, 30, 2, '2020-12-24', '2021-03-24', '2020-09-24 22:53:59', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', 'C', '', '0000-00-00 00:00:00'),
(121, 6, 30, 3, '2021-03-24', '2021-06-24', '2020-09-24 22:53:59', 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 'Terimakasih Cinta2', 'untuk segalanya2', '', 'A', '', '2020-09-27 00:16:48'),
(122, 6, 31, 1, '2022-09-24', '2022-12-24', '2020-09-24 23:11:12', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(123, 6, 31, 2, '2022-12-24', '2023-03-24', '2020-09-24 23:11:12', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '0000-00-00 00:00:00'),
(124, 7, 32, 1, '2020-09-24', '2020-12-24', '0000-00-00 00:00:00', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 'Pergilah sayang', 'sayang', '', 'C', '', '2020-09-26 13:54:45'),
(125, 7, 32, 2, '2020-12-24', '2021-03-24', '2020-09-24 23:23:59', 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 'OK', 'PRAkerja Bous', 'Iya', '', 'Diangkat menjadi karyawan tetap setelah habis masa kerja kontrak.', '2020-09-26 13:49:20'),
(126, 6, 30, 4, '2021-06-24', '2021-09-24', '2020-09-26 11:56:59', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 'INI 1 2', 'Ini 2 2', 'Ini 3 2', '', 'Diperpanjang kontrak selama 3 Bulan', '2020-09-27 00:30:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL,
  `email` varchar(60) NOT NULL,
  `login_status` int(1) NOT NULL,
  `password` varchar(300) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `departemen` varchar(5) NOT NULL,
  `posisi` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(6) NOT NULL,
  `foto` varchar(300) NOT NULL,
  `tgl_daftar` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `login_status`, `password`, `nama`, `no_hp`, `tgl_lahir`, `departemen`, `posisi`, `jenis_kelamin`, `foto`, `tgl_daftar`, `trn_date`) VALUES
(1, '', 0, '', '', '', '0000-00-00', '', '', '', '', '2020-07-30 06:51:40', '0000-00-00 00:00:00'),
(2, 'user@gmail.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'User', '08999888877', '2002-02-02', 'HC', 'Human Capital', 'Female', '20200726120359Small Malious.png', '2020-07-26 10:03:59', '0000-00-00 00:00:00'),
(3, 'admin2@gmail.com', 1, '200820e3227815ed1756a6b531e7e0d2', 'Admin2', '08979998888', '1995-05-05', 'HCM', 'Human Capital Ciputra', 'Male', 'Save Earth.jpg', '2020-07-18 12:31:22', '0000-00-00 00:00:00'),
(4, 'dahlia@ciputra.com', 1, '200820e3227815ed1756a6b531e7e0d2', 'Dahlia', '08977778888', '1993-03-03', 'HCGA', 'HC OFFICER', 'Female', '', '2020-07-18 14:47:55', '0000-00-00 00:00:00'),
(5, 'admin@gmail.com', 1, '46f94c8de14fb36680850768ff1b7f2a', 'Admin', '08979100048', '2000-12-19', 'CONS', 'HC OFFICER', 'Female', '20200718212140pas foto.jpg', '2020-07-30 06:51:33', '0000-00-00 00:00:00'),
(11, 'hendrik@ciputra.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Hendrik', '08777889990', '1995-05-05', 'EM', 'DIRECTOR', 'Male', '20200726122553instagram.png', '2020-07-26 10:25:53', '0000-00-00 00:00:00'),
(12, 'abdul@ciputra.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Abdul', '08777666888', '1992-02-02', 'LND', 'ESTATE MANAGEMENT DEPUTY MANAGER', 'Male', '', '2020-07-19 09:06:45', '0000-00-00 00:00:00'),
(13, 'maryulianti26@gmail.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Ria Yulia', '081315708221', '1996-07-22', 'FA', 'ASSISTANT ACCOUNTING MANAGER', 'Female', '', '2020-08-04 16:52:25', '0000-00-00 00:00:00'),
(14, 'agunggumelar1912@gmail.com', 2, '200820e3227815ed1756a6b531e7e0d2', 'Agung Gumelar', '08979100048', '1997-12-19', 'CONS', 'WEB DEVELOPER', 'Male', '20200926061900pas foto.png', '2020-09-26 04:19:00', '2020-08-06 18:41:06');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `viewass`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `viewass` (
`id_karyawan` int(10)
,`status_penilaian` varchar(17)
,`status_kontrak` int(1)
,`nama_karyawan` varchar(60)
,`nik` varchar(10)
,`tgl_masuk` date
,`lokasi` varchar(60)
,`departemen_karyawan` varchar(30)
,`posisi_karyawan` varchar(50)
,`golongan` varchar(2)
,`nama` varchar(60)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `assproses`
--
DROP TABLE IF EXISTS `assproses`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `assproses`  AS  select `a`.`id` AS `id_penilaian`,`a`.`status_penilaian` AS `status_penilaian`,`a`.`hasil` AS `hasil`,`b`.`id` AS `id_kontrak`,`b`.`status_kontrak` AS `status_kontrak`,`b`.`banyak_penilaian` AS `banyak_penilaian`,`c`.`nama_karyawan` AS `nama_karyawan`,`c`.`nik` AS `nik` from ((`tb_penilaian` `a` join `tb_kontrak` `b` on(`a`.`id_kontrak` = `b`.`id`)) join `tb_karyawan` `c` on(`a`.`id_karyawan` = `c`.`id`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `assuser`
--
DROP TABLE IF EXISTS `assuser`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `assuser`  AS  select `a`.`id` AS `id`,`a`.`id_user1` AS `id_user1`,`a`.`nama_karyawan` AS `nama_karyawan`,`a`.`nik` AS `nik`,`b`.`id` AS `id_kontrak`,`b`.`status_kontrak` AS `status_kontrak`,`b`.`banyak_penilaian` AS `banyak_penilaian`,`c`.`id` AS `id_penilaian`,`c`.`status_penilaian` AS `status_penilaian` from ((`tb_karyawan` `a` join `tb_kontrak` `b` on(`a`.`id` = `b`.`id_karyawan`)) join `tb_penilaian` `c` on(`b`.`id` = `c`.`id_kontrak`)) where `b`.`id_proses` = 0 and `c`.`hasil` = '' order by `b`.`id` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `editass_karyawan`
--
DROP TABLE IF EXISTS `editass_karyawan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `editass_karyawan`  AS  select `a`.`id` AS `id`,`a`.`nik` AS `nik`,`a`.`nama_karyawan` AS `nama_karyawan`,`a`.`tgl_masuk` AS `tgl_masuk`,`a`.`lokasi` AS `lokasi`,`a`.`departemen_karyawan` AS `departemen_karyawan`,`a`.`posisi_karyawan` AS `posisi_karyawan`,`a`.`golongan` AS `golongan`,`a`.`id_user1` AS `id_user1`,`a`.`id_user2` AS `id_user2`,`b`.`nama` AS `nama`,`b`.`departemen` AS `departemen`,`c`.`nama` AS `nama2`,`c`.`departemen` AS `departemen2` from ((`tb_karyawan` `a` join `tb_user` `b` on(`a`.`id_user1` = `b`.`id`)) join `tb_user` `c` on(`a`.`id_user2` = `c`.`id`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `editass_kontrak`
--
DROP TABLE IF EXISTS `editass_kontrak`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `editass_kontrak`  AS  select `a`.`id` AS `id`,`a`.`mulai_penilaian` AS `mulai_penilaian`,`a`.`selesai_penilaian` AS `selesai_penilaian`,`a`.`hasil` AS `hasil`,`a`.`id_kontrak` AS `id_kontrak`,`a`.`status_penilaian` AS `status_penilaian`,`b`.`nama_karyawan` AS `nama_karyawan`,`b`.`nik` AS `nik`,`b`.`tgl_masuk` AS `tgl_masuk`,`c`.`status_kontrak` AS `status_kontrak`,`c`.`banyak_penilaian` AS `banyak_penilaian` from ((`tb_penilaian` `a` join `tb_karyawan` `b` on(`a`.`id_karyawan` = `b`.`id`)) join `tb_kontrak` `c` on(`a`.`id_kontrak` = `c`.`id`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `printass`
--
DROP TABLE IF EXISTS `printass`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `printass`  AS  select `a`.`id` AS `id_penilaian`,`a`.`id` AS `id`,`a`.`id_karyawan` AS `id_karyawan`,`a`.`id_kontrak` AS `id_kontrak`,`a`.`status_penilaian` AS `status_penilaian`,`a`.`mulai_penilaian` AS `mulai_penilaian`,`a`.`selesai_penilaian` AS `selesai_penilaian`,`a`.`tgl_buat` AS `tgl_buat`,`a`.`jawaban1` AS `jawaban1`,`a`.`jawaban2` AS `jawaban2`,`a`.`jawaban3` AS `jawaban3`,`a`.`jawaban4` AS `jawaban4`,`a`.`jawaban5` AS `jawaban5`,`a`.`jawaban6` AS `jawaban6`,`a`.`jawaban7` AS `jawaban7`,`a`.`jawaban8` AS `jawaban8`,`a`.`jawaban9` AS `jawaban9`,`a`.`jawaban10` AS `jawaban10`,`a`.`jawaban11` AS `jawaban11`,`a`.`ket1` AS `ket1`,`a`.`ket2` AS `ket2`,`a`.`ket3` AS `ket3`,`a`.`hasil` AS `hasil`,`a`.`rekomendasi` AS `rekomendasi`,`a`.`tgl_penilaian` AS `tgl_penilaian`,`b`.`mulai_kontrak` AS `mulai_kontrak`,`b`.`selesai_kontrak` AS `selesai_kontrak`,`b`.`banyak_penilaian` AS `banyak_penilaian`,`b`.`status_kontrak` AS `status_kontrak`,`c`.`nik` AS `nik`,`c`.`nama_karyawan` AS `nama_karyawan`,`c`.`tgl_masuk` AS `tgl_masuk`,`c`.`lokasi` AS `lokasi`,`c`.`departemen_karyawan` AS `departemen_karyawan`,`c`.`posisi_karyawan` AS `posisi_karyawan`,`c`.`golongan` AS `golongan`,`c`.`tgl_buat` AS `tgl_buat_karyawan`,`d`.`nama` AS `nama` from (((`tb_penilaian` `a` join `tb_kontrak` `b` on(`a`.`id_kontrak` = `b`.`id`)) join `tb_karyawan` `c` on(`c`.`id` = `b`.`id_karyawan`)) join `tb_user` `d` on(`c`.`id_user1` = `d`.`id`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `viewass`
--
DROP TABLE IF EXISTS `viewass`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewass`  AS  select `a`.`id_karyawan` AS `id_karyawan`,if(`a`.`status_penilaian` = '1' and `a`.`mulai_penilaian` = '0000-00-00','On Progress Admin',`a`.`status_penilaian`) AS `status_penilaian`,`b`.`status_kontrak` AS `status_kontrak`,`c`.`nama_karyawan` AS `nama_karyawan`,`c`.`nik` AS `nik`,`c`.`tgl_masuk` AS `tgl_masuk`,`c`.`lokasi` AS `lokasi`,`c`.`departemen_karyawan` AS `departemen_karyawan`,`c`.`posisi_karyawan` AS `posisi_karyawan`,`c`.`golongan` AS `golongan`,`d`.`nama` AS `nama` from (((`tb_penilaian` `a` join `tb_kontrak` `b` on(`a`.`id_kontrak` = `b`.`id`)) join `tb_karyawan` `c` on(`a`.`id_karyawan` = `c`.`id`)) join `tb_user` `d` on(`c`.`id_user1` = `d`.`id`)) where `a`.`id` in (select max(`tb_penilaian`.`id`) from `tb_penilaian` group by `tb_penilaian`.`id_karyawan`) order by `c`.`nama_karyawan` ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `password_reset_temp`
--
ALTER TABLE `password_reset_temp`
  ADD KEY `expDate` (`expDate`);

--
-- Indeks untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`),
  ADD KEY `id_user1` (`id_user1`,`id_user2`);

--
-- Indeks untuk tabel `tb_kontrak`
--
ALTER TABLE `tb_kontrak`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_karyawan` (`id_karyawan`);

--
-- Indeks untuk tabel `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_karyawan` (`id_karyawan`,`id_kontrak`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_kontrak`
--
ALTER TABLE `tb_kontrak`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_kontrak`
--
ALTER TABLE `tb_kontrak`
  ADD CONSTRAINT `tb_kontrak_ibfk_1` FOREIGN KEY (`id_karyawan`) REFERENCES `tb_karyawan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
  ADD CONSTRAINT `tb_penilaian_ibfk_1` FOREIGN KEY (`id_karyawan`) REFERENCES `tb_karyawan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
