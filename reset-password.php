<?php
include 'koneksi.php';
if (isset($_GET["key"]) && isset($_GET["email"]) && isset($_GET["action"]) && ($_GET["action"]=="reset") && !isset($_POST["action"])){
  $key = $_GET["key"];
  $email = $_GET["email"];
  date_default_timezone_set('Asia/Jakarta');
  $curDate = date("Y-m-d H:i:s");

  $sql = "SELECT * FROM `password_reset_temp` WHERE `key`='".$key."' and `email`='".$email."';";
  $qry = mysqli_query($koneksi, $sql) or die ("Query Key dan Email salah!");
  $row = mysqli_num_rows($qry);
  if ($row==""){
    $error = '<h2>Invalid Link</h2><p>The link is invalid/expired. Either you did not copy the correct linkfrom the email, or you have already used the key in which case it is deactivated.</p><p><a href="https://localhost/penilaian/lupapass.php">Click here</a> to reset password.</p>';
  }else{
    $row = mysqli_fetch_assoc($qry);
    $expDate = $row['expDate'];
    if ($expDate >= $curDate){
      $error = "";
      ?>
      <!DOCTYPE html>
      <html>
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/font-awesome/css/all.min.css" type="text/css">

        <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
        <title>Ciputra</title>


      </head>

      <body class="bg-color-1">
        <div>
          <form class="form" name="update" action="" method="post">
            <input type="hidden" name="action" value="update" />
            <div class="align-middle">
              <div class="tengah2 kotak_tengah2">
                </br>
                <center><i class="fas fa-unlock-alt fa-5x"></i></center><br>
                <center><h4 style="line-height: 18px;">Recovery Password</h4></center>
                <center><font size="2">Please, create a new password</font></center>
                </br>
                <div class="input-group mb-3">
                  <div class="input-group-append">
                    <span class="input-group-text warna-icon-login"><i class="fas fa-lock"></i></span>
                  </div>
                  <input required type="password" name="pass1" class="form-control input_user border-list" placeholder="New Password" autocomplete="off">
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-append">
                    <span class="input-group-text warna-icon-login"><i class="fas fa-lock"></i></span>
                  </div>
                  <input required type="password" name="pass2" class="form-control input_user border-list" placeholder="Comfirmation Password" autocomplete="off">
                </div>
                </br>

                <table width="100%">
                  <tr>
                    <td style="border: 0; padding: 0px;">
                      <input type="hidden" name="email" value="<?php echo $email;?>"/>
                      <button type="submit" name="submit" class="btn btn-primary button-right">Send</button>
                      <a href="login.php" class="btn btn-dark button-left button-space">Back</a>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </form>
        </div>


      <script src="assets/js/jquery.js"></script> 
      <script src="assets/js/popper.js"></script> 
      <script src="assets/js/bootstrap.js"></script>
      </body>
      </html>

      <br />
    <?php
    }else{
      $error = "<h2>Link Expired</h2><p>The link is expired. You are trying to use the expired link which as valid only 24 hours (1 days after request).<br /><br /></p>";
    }
  }

  if($error!=""){
    echo "<div class='error'>".$error."</div><br />";
  } 
} // isset email key validate end


if(isset($_POST["email"]) && isset($_POST["action"]) && ($_POST["action"]=="update")){
  $error="";
  $pass1 = mysqli_real_escape_string($koneksi,$_POST["pass1"]);
  $pass2 = mysqli_real_escape_string($koneksi,$_POST["pass2"]);
  $email = $_POST["email"];
  $curDate = date("Y-m-d H:i:s");
  $alfabet = preg_match('@[a-zA-Z]@', $pass1);
  $number = preg_match('@[0-9]@', $pass1);

  if (!$alfabet ||  !$number || strlen($pass1) <= 5) {
    echo "<script>alert('Password  harus mengandung angka dan huruf & lebih dari 6 character!');history.go(-1)</script>";
  }elseif ($pass1!=$pass2){
    echo "<script>alert('Confirmation Password tidak sama!');history.go(-1)</script>";
    // $error.= "<p>Password do not match, both password should be same.<br /><br /></p>";
  // }elseif($error!=""){
  //   echo "<div class='error'>".$error."</div><br />";
  }else{
    $pass1 = md5($pass1);
    $sql2 = "UPDATE tb_user SET password='$pass1', trn_date='$curDate' WHERE email='$email'";
    $qry2 = mysqli_query($koneksi, $sql2) or die("Query Update salah!");
    
    $sql3 = "DELETE FROM password_reset_temp WHERE email='$email'";
    $qry3 = mysqli_query($koneksi,$sql3) or die ("Query Delete salah!");

    echo "<script>alert('Selamat! Password kamu telah berhasil dibuat ulang, Silahkan Login.');window.location='login.php'; </script>";
  } 
}
?>