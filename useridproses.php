<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" type="image/x-icon" href="gambar/logociputra.svg">
  <title>Ciputra</title>
  <!-- css manual -->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php
  include 'koneksi.php';

  // mengaktifkan session
  session_start();
  if (!isset($_SESSION['adminlogin'])) {
  // if($_SESSION['status'] != "login") {
    echo '<script language="javascript">alert("Dilarang akses, login sebagai admin terlebih dahulu"); location.href="logout.php"</script>';
  }

  $sql = "SELECT nama, email, foto FROM tb_user WHERE email='$_SESSION[adminlogin]'";
  $qry = mysqli_query($koneksi, $sql) or die ("Query user salah!");
  $row = mysqli_fetch_array($qry);

  $id = $_GET['id'];
  $sql2 = "SELECT nama, no_hp, tgl_lahir, departemen, posisi, email, jenis_kelamin FROM tb_user WHERE id='$id'";
  $qry2 = mysqli_query($koneksi, $sql2);
  $row2 = mysqli_fetch_array($qry2);
  $tgl_lahir = date("d-m-Y", strtotime($row2['tgl_lahir']));
  $p = $row2['posisi'];

  if (isset($_POST['update'])) {
    $nama = trim($_POST['nama']);
    $no_hp = trim($_POST['no_hp']);
    $tgl_lahir2 = date("Y-m-d", strtotime($_POST['tgl_lahir']));
    $departemen = trim($_POST['departemen']);
    $posisi = trim($_POST['posisi']);
    $email = trim($_POST['email']);
    $jenis_kelamin = trim($_POST['jenis_kelamin']);

    $cek = "SELECT email FROM tb_user WHERE email='$email'";
    $cekqry = mysqli_query($koneksi, $cek);
    $cekrow = mysqli_fetch_array($cekqry);

    if (preg_match("/HC OFFICER/", $posisi)) {
      echo $login_status = "1";
    }else{
      echo $login_status = "2";
    }

    if ($row2['nama'] == $nama) {
      if ($row2['no_hp'] == $no_hp) {
        if ($row2['tgl_lahir'] == $tgl_lahir2) {
          if ($row2['departemen'] == $departemen) {
            if ($row2['posisi'] == $posisi) {
              if ($row2['email'] == $email) {
                if ($row2['jenis_kelamin'] == $jenis_kelamin) {
                  echo "<script>alert('Tidak ada data yang berubah!');window.location='updateuserid.php'; </script>";
                }
              }
            }
          }
        }
      }
    }

    if (empty($nama) && empty($no_hp) && empty($tgl_lahir2) && empty($departemen) && empty($posisi) && empty($email) && empty($jenis_kelamin)) {
      echo "<script>alert('Data tidak boleh kosong!');history.go(-1)</script>";
    }elseif (empty($nama)) {
      echo "<script>alert('Name harus di isi!');history.go(-1)</script>";
    }elseif (empty($no_hp)) {
      echo "<script>alert('Number Phone harus di isi!');history.go(-1)</script>";
    }elseif (empty($tgl_lahir2)) {
      echo "<script>alert('Date of Birth harus di isi!');history.go(-1)</script>";
    }elseif (empty($departemen)) {
      echo "<script>alert('Departement harus di isi!');history.go(-1)</script>";
    }elseif (empty($posisi)) {
      echo "<script>alert('Silahkan pilih Position!');history.go(-1)</script>";
    }elseif (empty($email)) {
      echo "<script>alert('Email harus di isi!!');history.go(-1)</script>";  
    }elseif (empty($jenis_kelamin)) {
      echo "<script>alert('Silahkan pilih Gender!');history.go(-1)</script>";
    }elseif (!preg_match("/^[a-zA-Z ]*$/", $nama)) {
      echo "<script>alert('Name tidak boleh menganduk special char dan angka!');history.go(-1)</script>";
    }elseif (strlen($nama) >= 60) {
      echo "<script>alert('Panjang Name tidak boleh 60 Character!');history.go(-1)</script>";
    }elseif (!preg_match("/^[0-9]*$/", $no_hp)) {
      echo "<script>alert('Number Phone tidak boleh mengandung special character atau huruf!');history.go(-1)</script>";
    }elseif (strlen($no_hp) <= 9 || strlen($no_hp) >= 14) {
      echo "<script>alert('Number Phone hanya boleh 10-13 Angka!');history.go(-1)</script>";
    }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      echo "<script>alert('Format email yang anda masukkan salah!');history.go(-1)</script>";
    }elseif ($jenis_kelamin != "Female" && $jenis_kelamin != "Male") {
      echo "<script>alert('Gender Salah!');history.go(-1)</script>";
    }else{
      $sqlupdate = "UPDATE tb_user SET nama='$nama', no_hp='$no_hp', tgl_lahir='$tgl_lahir2', departemen='$departemen', posisi='$posisi', email='$email', jenis_kelamin='$jenis_kelamin', login_status='$login_status' WHERE id='$id'";
      $hasil = mysqli_query ($koneksi, $sqlupdate) or die ("query update salah");

      echo "<script>alert('Data User Id telah terupdate.');window.location='updateuserid.php'; </script>";
    }
  }



  ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span><?php echo $_SESSION['adminlogin']; ?></span>
          <i class="fas fa-user-alt"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <a href="setting.php" class="dropdown-item">
            <i class="fas fa-cog mr-2"></i>
            <span class="float-right text-muted text-sm">Setting</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt mr-2"></i>
            <span class="float-right text-muted text-sm">Logout</span>
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-olive elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link navbar-light">
      <img src="gambar/logociputra.svg" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>CIPUTRA</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php
            $cek_foto = $row['foto'];
            $tempat_foto = 'foto/'.$row['foto']; 
            if ($cek_foto) {
              echo "<img src='$tempat_foto' class='img-circle elevation-2' alt='User Image'>"; 
            }else{
              echo "<img src='foto/blank.png'></a>";
            }
          ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $row['nama']; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="createass.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Create Assessment
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="viewass.php" class="nav-link">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                View Assessment
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage User Id
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="adduserid.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User Id</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="updateuserid.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update User Id</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="setting.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update User Id</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item"><a href="updateuserid.php">Manage User Id</a></li>
              <li class="breadcrumb-item"><a href="updateuserid.php">Update User Id</a></li>
              <li class="breadcrumb-item active">Update Process</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-olive">
          <div class="card-header">
            <h3 class="card-title">Update User Id Process</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <form action="" method="post">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Name</label>
                    <input required name="nama" type="text" class="form-control border-list-olive" value="<?php echo $row2['nama']; ?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Number Phone</label>
                      <div class="input-group">
                        <input required minlength="11" maxlength="14" name="no_hp" type="text" class="form-control border-list-olive" value="<?php echo $row2['no_hp']; ?>">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Date of Birth</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input required name="tgl_lahir" type="text" class="form-control datetimepicker-input" data-target="#reservationdate" data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy" data-mask value="<?php echo $tgl_lahir; ?>">
                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Departement</label>
                    <select required name="departemen" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="<?php echo $row2['departemen']; ?>"><?php echo $row2['departemen']; ?></option>
                      <option value="BD" <?php if($row2['departemen'] == "BD") echo "selected";?>>BD</option>
                      <option value="CONS" <?php if($row2['departemen'] == "CONS") echo "selected";?>>CONS</option>
                      <option value="DGMKT" <?php if($row2['departemen'] == "DGMKT") echo "selected";?>>DGMKT</option>
                      <option value="EM" <?php if($row2['departemen'] == "EM") echo "selected";?>>EM</option>
                      <option value="FA" <?php if($row2['departemen'] == "FA") echo "selected";?>>FA</option>
                      <option value="HCGA" <?php if($row2['departemen'] == "HCGA") echo "selected";?>>HCGA</option>
                      <option value="LEGP" <?php if($row2['departemen'] == "LEGP") echo "selected";?>>LEGP</option>
                      <option value="LND" <?php if($row2['departemen'] == "LND") echo "selected";?>>LND</option>
                      <option value="MCOM" <?php if($row2['departemen'] == "MCOM") echo "selected";?>>MCOM</option>
                      <option value="MIS" <?php if($row2['departemen'] == "MIS") echo "selected";?>>MIS</option>
                      <option value="MKTDA" <?php if($row2['departemen'] == "MKTDA") echo "selected";?>>MKTDA</option>
                      <option value="PDG" <?php if($row2['departemen'] == "PDG") echo "selected";?>>PDG</option>
                      <option value="QS" <?php if($row2['departemen'] == "QS") echo "selected";?>>QS</option>
                      <option value="SALES" <?php if($row2['departemen'] == "SALES") echo "selected";?>>SALES</option>
                   </select>
                 </div>
                 <!-- /.form-group -->
               </div>
               <!-- /.col -->
               <div class="col-12 col-sm-6">
                 <!-- /.form-group -->
                 <div class="form-group">
                   <label>Position</label>
                   <select required name="posisi" class="form-control select2 border-list-olive" style="width: 100%;">
                      <option value="ACCOUNTING ADMINISTRATOR" <?php if($p == "ACCOUNTING ADMINISTRATOR") echo "selected";?>>ACCOUNTING ADMINISTRATOR</option>
                      <option value="ACCOUNTING SUPERVISOR" <?php if($p == "ACCOUNTING SUPERVISOR") echo "selected";?>>ACCOUNTING SUPERVISOR</option>
                      <option value="ADMIN ACCONTING" <?php if($p == "ADMIN ACCOUNTING") echo "selected";?>>ADMIN ACCONTING</option>
                      <option value="ADMIN COLLECTION" <?php if($p == "ADMIN COLLECTION") echo "selected";?>>ADMIN COLLECTION</option>
                      <option value="ADMIN PURNA JUAL" <?php if($p == "ADMIN PURNA JUAL") echo "selected";?>>ADMIN PURNA JUAL</option>
                      <option value="AFTER SALES ADMINISTATION" <?php if($p == "AFTER SALES ADMINISTATION") echo "selected";?>>AFTER SALES ADMINISTATION</option>
                      <option value="AFTER SALES SUPERVISOR" <?php if($p == "AFTER SALES SUPERVISOR") echo "selected";?>>AFTER SALES SUPERVISOR</option>
                      <option value="ARCHITECT" <?php if($p == "ARCHITECT") echo "selected";?>>ARCHITECT</option>
                      <option value="ASSISTANT ACCOUNTING MANAGER" <?php if($p == "ASSISTANT ACCOUNTING MANAGER") echo "selected";?>>ASSISTANT ACCOUNTING MANAGER</option>
                      <option value="ASSISTANT COLLECTION MANAGER" <?php if($p == "ASSISTANT COLLECTION MANAGER") echo "selected";?>>ASSISTANT COLLECTION MANAGER</option>
                      <option value="ASSISTANT ESTATE MANAGEMENT MANAGER" <?php if($p == "ASSISTANT ESTATE MANAGEMENT MANAGER") echo "selected";?>>ASSISTANT ESTATE MANAGEMENT MANAGER</option>
                      <option value="ASSISTANT QUANTITY SURVEYOR MANAGER" <?php if($p == "ASSISTANT QUANTITY SURVEYOR MANAGER") echo "selected";?>>ASSISTANT QUANTITY SURVEYOR MANAGER</option>
                      <option value="ASSOCIATE DIRECTOR" <?php if($p == "ASSOCIATE DIRECTOR") echo "selected";?>>ASSOCIATE DIRECTOR</option>
                      <option value="ASSOCIATE DIRECTOR 1" <?php if($p == "ASSOCIATE DIRECTOR 1") echo "selected";?>>ASSOCIATE DIRECTOR 1</option>
                      <option value="BUILDING COORDINATOR" <?php if($p == "BUILDING COORDINATOR") echo "selected";?>>BUILDING COORDINATOR</option>
                      <option value="BUILDING INSPECTOR" <?php if($p == "BUILDING INSPECTOR") echo "selected";?>>BUILDING INSPECTOR</option>
                      <option value="BUILDING SITE MANAGER" <?php if($p == "BUILDING SITE MANAGER") echo "selected";?>>BUILDING SITE MANAGER</option>
                      <option value="BUSINESS DEVELOPMENT OFFICER" <?php if($p == "BUSINESS DEVELOPMENT OFFICER") echo "selected";?>>BUSINESS DEVELOPMENT OFFICER</option>
                      <option value="CASHIRE" <?php if($p == "CASHIRE") echo "selected";?>>CASHIRE</option>
                      <option value="COLLECTION ADMINISTRATOR" <?php if($p == "COLLECTION ADMINISTRATOR") echo "selected";?>>COLLECTION ADMINISTRATOR</option>
                      <option value="COLLECTION OFFICE" <?php if($p == "COLLECTION OFFICE") echo "selected";?>>COLLECTION OFFICE</option>
                      <option value="COMMERCIAL AREA INSPECTOR" <?php if($p == "COMMERCIAL AREA INSPECTOR") echo "selected";?>>COMMERCIAL AREA INSPECTOR</option>
                      <option value="CONSTRUCTION ADMINISTRATOR" <?php if($p == "CONSTRUCTION ADMINISTRATOR") echo "selected";?>>CONSTRUCTION ADMINISTRATOR</option>
                      <option value="CONSTRUCTION MANAGER" <?php if($p == "CONSTRUCTION MANAGER") echo "selected";?>>CONSTRUCTION MANAGER</option>
                      <option value="CUSTOMER SERVICE" <?php if($p == "CUSTOMER SERVICE") echo "selected";?>>CUSTOMER SERVICE</option>
                      <option value="DESIGN GRAPHIC" <?php if($p == "DESIGN GRAPHIC") echo "selected";?>>DESIGN GRAPHIC</option>
                      <option value="DIGITAL MARKETING MANAGER" <?php if($p == "DIGITAL MARKETING MANAGER") echo "selected";?>>DIGITAL MARKETING MANAGER</option>
                      <option value="DIGITAL MARKETING OFFICER" <?php if($p == "DIGITAL MARKETING OFFICER") echo "selected";?>>DIGITAL MARKETING OFFICER</option>
                      <option value="DIRECTOR" <?php if($p == "DIRECTOR") echo "selected";?>>DIRECTOR</option>
                      <option value="DRAFTER" <?php if($p == "DRAFTER") echo "selected";?>>DRAFTER</option>
                      <option value="DRIVER" <?php if($p == "DRIVER") echo "selected";?>>DRIVER</option>
                      <option value="ENVIRONMENT MAINTENANCE INSPECTOR" <?php if($p == "ENVIRONMENT MAINTENANCE INSPECTOR") echo "selected";?>>ENVIRONMENT MAINTENANCE INSPECTOR</option>
                      <option value="ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR" <?php if($p == "ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR") echo "selected";?>>ENVIRONMENT MAINTENANCE INSPECTOR SUPERVISOR</option>
                      <option value="ESTATE COORDINATOR" <?php if($p == "ESTATE COORDINATOR") echo "selected";?>>ESTATE COORDINATOR</option>
                      <option value="ESTATE MANAGEMENT ADMINISTRATOR" <?php if($p == "ESTATE MANAGEMENT ADMINISTRATOR") echo "selected";?>>ESTATE MANAGEMENT ADMINISTRATOR</option>
                      <option value="ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD" <?php if($p == "ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD") echo "selected";?>>ESTATE MANAGEMENT AGREEMENT & FINANCE SECTION HEAD</option>
                      <option value="ESTATE MANAGEMENT DEPUTY MANAGER" <?php if($p == "ESTATE MANAGEMENT DEPUTY MANAGER") echo "selected";?>>ESTATE MANAGEMENT DEPUTY MANAGER</option>
                      <option value="FINANCE & ACCOUNTING DEPUTY MANAGER" <?php if($p == "FINANCE & ACCOUNTING DEPUTY MANAGER") echo "selected";?>>FINANCE & ACCOUNTING DEPUTY MANAGER</option>
                      <option value="FINANCE & ACCOUNTING GENERAL MANAGER" <?php if($p == "FINANCE & ACCOUNTING GENERAL MANAGER") echo "selected";?>>FINANCE & ACCOUNTING GENERAL MANAGER</option>
                      <option value="FINANCE & ACCOUNTING MANAGER" <?php if($p == "FINANCE & ACCOUNTING MANAGER") echo "selected";?>>FINANCE & ACCOUNTING MANAGER</option>
                      <option value="FINANCE ADMINISTRATOR" <?php if($p == "FINANCE ADMINISTRATOR") echo "selected";?>>FINANCE ADMINISTRATOR</option>
                      <option value="FINANCE, ACCOUNTING & TAX DEPUTY MANAGER" <?php if($p == "FINANCE, ACCOUNTING & TAX DEPUTY MANAGER") echo "selected";?>>FINANCE, ACCOUNTING & TAX DEPUTY MANAGER</option>
                      <option value="GENERAL AFFAIR SECTION HEAD" <?php if($p == "GENERAL AFFAIR SECTION HEAD") echo "selected";?>>GENERAL AFFAIR SECTION HEAD</option>
                      <option value="GM OPERATIONAL" <?php if($p == "GM OPERATIONAL") echo "selected";?>>GM OPERATIONAL</option>
                      <option value="HC & GA DEPUTY GM" <?php if($p == "HC & GA DEPUTY GM") echo "selected";?>>HC & GA DEPUTY GM</option>
                      <option value="HC & GA MANAGER" <?php if($p == "HC & GA MANAGER") echo "selected";?>>HC & GA MANAGER</option>
                      <option value="HC ADMINISTRATOR" <?php if($p == "HC ADMINISTRATOR") echo "selected";?>>HC ADMINISTRATOR</option>
                      <option value="HC OFFICER" <?php if($p == "HC OFFICER") echo "selected";?>>HC OFFICER</option>
                      <option value="HSE INSPECTOR" <?php if($p == "HSE INSPECTOR") echo "selected";?>>HSE INSPECTOR</option>
                      <option value="HSE OFFICER" <?php if($p == "HSE OFFICER") echo "selected";?>>HSE OFFICER</option>
                      <option value="INFRASTRUCTURE INSPECTOR" <?php if($p == "INFRASTRUCTURE INSPECTOR") echo "selected";?>>INFRASTRUCTURE INSPECTOR</option>
                      <option value="INFRASTRUCTURE SITE MANAGER" <?php if($p == "INFRASTRUCTURE SITE MANAGER") echo "selected";?>>INFRASTRUCTURE SITE MANAGER</option>
                      <option value="INVOICE ADMINISTRATION" <?php if($p == "INVOICE ADMINISTRATION") echo "selected";?>>INVOICE ADMINISTRATION</option>
                      <option value="LAND ADMINISTRATOR"<?php if($p == "LAND ADMINISTRATOR") echo "selected";?>>LAND ADMINISTRATOR</option>
                      <option value="LAND MANAGER" <?php if($p == "LAND MANAGER") echo "selected";?>>LAND MANAGER</option>
                      <option value="LAND OFFICER" <?php if($p == "LAND OFFICER") echo "selected";?>>LAND OFFICER</option>
                      <option value="LAND SECTION HEAD" <?php if($p == "LAND SECTION HEAD") echo "selected";?>>LAND SECTION HEAD</option>
                      <option value="LAND SURVEYOR" <?php if($p == "LAND SURVEYOR") echo "selected";?>>LAND SURVEYOR</option>
                      <option value="LANDSCAPE DESIGN" <?php if($p == "LANDSCAPE DESIGN") echo "selected";?>>LANDSCAPE DESIGN</option>
                      <option value="LANDSCAPE INSPECTOR" <?php if($p == "LANDSCAPE INSPECTOR") echo "selected";?>>LANDSCAPE INSPECTOR</option>
                      <option value="LANDSCAPE SECTION HEAD" <?php if($p == "LANDSCAPE SECTION HEAD") echo "selected";?>>LANDSCAPE SECTION HEAD</option>
                      <option value="LEGAL & PERMIT MANAGER" <?php if($p == "LEGAL & PERMIT MANAGER") echo "selected";?>>LEGAL & PERMIT MANAGER</option>
                      <option value="LEGAL ADMINISTRATOR" <?php if($p == "LEGAL ADMINISTRATOR") echo "selected";?>>LEGAL ADMINISTRATOR</option>
                      <option value="LEGAL OFFICER" <?php if($p == "LEGAL OFFICER") echo "selected";?>>LEGAL OFFICER</option>
                      <option value="LEGAL SECTION HEAD" <?php if($p == "LEGAL SECTION HEAD") echo "selected";?>>LEGAL SECTION HEAD</option>
                      <option value="LIFE GUARD" <?php if($p == "LIFE GUARD") echo "selected";?>>LIFE GUARD</option>
                      <option value="MANAGEMENT INFORMATION SYSTEMS MANAGER" <?php if($p == "MANAGEMENT INFORMATION SYSTEMS MANAGER") echo "selected";?>>MANAGEMENT INFORMATION SYSTEMS MANAGER</option>
                      <option value="MARKETING MANAGER" <?php if($p == "MARKETING MANAGER") echo "selected";?>>MARKETING MANAGER</option>
                      <option value="MECHANICAL ELECTRICAL INSPECTOR" <?php if($p == "MECHANICAL ELECTRICAL INSPECTOR") echo "selected";?>>MECHANICAL ELECTRICAL INSPECTOR</option>
                      <option value="MECHANICAL ELECTRICAL SECTION HEAD" <?php if($p == "MECHANICAL ELECTRICAL SECTION HEAD") echo "selected";?>>MECHANICAL ELECTRICAL SECTION HEAD</option>
                      <option value="MECHANICAL ELECTRICAL SITE MANAGER" <?php if($p == "MECHANICAL ELECTRICAL SITE MANAGER") echo "selected";?>>MECHANICAL ELECTRICAL SITE MANAGER</option>
                      <option value="MESSENGER" <?php if($p == "MESSENGER") echo "selected";?>>MESSENGER</option>
                      <option value="MIS COORDINATOR" <?php if($p == "MIS COORDINATOR") echo "selected";?>>MIS COORDINATOR</option>
                      <option value="MIS OFFICER" <?php if($p == "MIS OFFICER") echo "selected";?>>MIS OFFICER</option>
                      <option value="OFFICE BOY" <?php if($p == "OFFICE BOY") echo "selected";?>>OFFICE BOY</option>
                      <option value="OPERATOR GENERAL MANAGER" <?php if($p == "OPERATOR GENERAL MANAGER") echo "selected";?>>OPERATOR GENERAL MANAGER</option>
                      <option value="OPERATOR HELPER" <?php if($p == "OPERATOR HELPER") echo "selected";?>>OPERATOR HELPER</option>
                      <option value="PAYROLL OFFICER" <?php if($p == "PAYROLL OFFICER") echo "selected";?>>PAYROLL OFFICER</option>
                      <option value="PENGAWAS BANGUNAN" <?php if($p == "PENGAWAS BANGUNAN") echo "selected";?>>PENGAWAS BANGUNAN</option>
                      <option value="PENGAWAS ME" <?php if($p == "PENGAWAS ME") echo "selected";?>>PENGAWAS ME</option>
                      <option value="PERMIT ADMINISTRATOR" <?php if($p == "PERMIT ADMINISTRATOR") echo "selected";?>>PERMIT ADMINISTRATOR</option>
                      <option value="PERMIT OFFICER" <?php if($p == "PERMIT OFFICER") echo "selected";?>>PERMIT OFFICER</option>
                      <option value="PLANNING & DESIGN GENERAL MANAGER" <?php if($p == "PLANNING & DESIGN GENERAL MANAGER") echo "selected";?>>PLANNING & DESIGN GENERAL MANAGER</option>
                      <option value="PLANNING & DESIGN MANAGER" <?php if($p == "PLANNING & DESIGN MANAGER") echo "selected";?>>PLANNING & DESIGN MANAGER</option>
                      <option value="PROMOTION ADMINISTRATOR" <?php if($p == "PROMOTION ADMINISTRATOR") echo "selected";?>>PROMOTION ADMINISTRATOR</option>
                      <option value="PROMOTION SUPERVISOR" <?php if($p == "PROMOTION SUPERVISOR") echo "selected";?>>PROMOTION SUPERVISOR</option>
                      <option value="PROMOTION SUPPORT" <?php if($p == "PROMOTION SUPPORT") echo "selected";?>>PROMOTION SUPPORT</option>
                      <option value="PURCHASING ADMINISTRATOR" <?php if($p == "PURCHASING ADMINISTRATOR") echo "selected";?>>PURCHASING ADMINISTRATOR</option>
                      <option value="QUALITY CONTROL" <?php if($p == "QUALITY CONTROL") echo "selected";?>>QUALITY CONTROL</option>
                      <option value="QUANTITY SURVEYOR ADMINISTRATOR" <?php if($p == "QUANTITY SURVEYOR ADMINISTRATOR") echo "selected";?>>QUANTITY SURVEYOR ADMINISTRATOR</option>
                      <option value="QUANTITY SURVEYOR ANALYST" <?php if($p == "QUANTITY SURVEYOR ANALYST") echo "selected";?>>QUANTITY SURVEYOR ANALYST</option>
                      <option value="QUANTITY SURVEYOR MANAGER" <?php if($p == "QUANTITY SURVEYOR MANAGER") echo "selected";?>>QUANTITY SURVEYOR MANAGER</option>
                      <option value="QUANTITY SURVEYOR OFFICER" <?php if($p == "QUANTITY SURVEYOR OFFICER") echo "selected";?>>QUANTITY SURVEYOR OFFICER</option>
                      <option value="SALES EXECUTIVE" <?php if($p == "SALES EXECUTIVE") echo "selected";?>>SALES EXECUTIVE</option>
                      <option value="SALES EXECUTIVE SUPERVISOR" <?php if($p == "SALES EXECUTIVE SUPERVISOR") echo "selected";?>>SALES EXECUTIVE SUPERVISOR</option>
                      <option value="SECURITY SECTION HEAD" <?php if($p == "SECURITY SECTION HEAD") echo "selected";?>>SECURITY SECTION HEAD</option>
                      <option value="SPORTCLUB, WW & WOW SECTION HEAD" <?php if($p == "SPORTCLUB, WW & WOW SECTION HEAD") echo "selected";?>>SPORTCLUB, WW & WOW SECTION HEAD</option>
                      <option value="SURVEYOR" <?php if($p == "SURVEYOR") echo "selected";?>>SURVEYOR</option>
                      <option value="TAX ADMINISTRATOR" <?php if($p == "TAX ADMINISTRATOR") echo "selected";?>>TAX ADMINISTRATOR</option>
                      <option value="TRAINER" <?php if($p == "TRAINER") echo "selected";?>>TRAINER</option>
                      <option value="WEB DEVELOPER" <?php if($p == "WEB DEVELOPER") echo "selected";?>>WEB DEVELOPER</option>
                      <option value="WTP OPERATOR" <?php if($p == "WTP OPERATOR") echo "selected";?>>WTP OPERATOR</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Email</label>
                    <input required name="email" type="text" class="form-control border-list-olive" value="<?php echo $row2['email'];?>">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                  <!-- /.form-group -->
                  <div class="form-group">
                    <label>Gender</label>
                    <select required name="jenis_kelamin" class="form-control select2" style="width: 100%;">
                      <option value="Male" <?php if($row2['jenis_kelamin'] == "Male") echo "selected";?>>Male</option>
                      <option value="Female" <?php if($row2['jenis_kelamin'] == "Female") echo "selected";?>>Female</option>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button onclick="return confirm('Apakah anda yakin ingin mengubah data User Id?')" name="update" type="submit" class="btn btn-primary float-right">Submit</button>
              <a href="updateuserid.php" class="btn btn-dark button-left button-space">&nbsp;Back&nbsp; </a>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#imageResult')
          .attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(function () {
    $('#upload').on('change', function () {
      readURL(input);
    });
  });

  var input = document.getElementById( 'upload' );
  var infoArea = document.getElementById( 'upload-label' );

  input.addEventListener( 'change', showFileName );
  function showFileName( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea.textContent = 'File name: ' + fileName;
  }
</script>
</body>
</html>